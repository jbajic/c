#!/bin/bash

# For unicode font type, export i_ps1_font_type="unicode"
if [ "$i_ps1_font_type" = "unicode" ]; then
    export i_ps1_char_branch=" "
    export i_ps1_char_clock=" "
    export i_ps1_char_error=" "
    export i_ps1_char_folder=" "
    export i_ps1_char_monitor=" "
    export i_ps1_char_prompt="$ "
    export i_ps1_char_refresh=" "
else
    export i_ps1_char_branch=""
    export i_ps1_char_clock=""
    export i_ps1_char_error="!"
    export i_ps1_char_folder=""
    export i_ps1_char_monitor=""
    export i_ps1_char_prompt="$ "
    export i_ps1_char_refresh="(r)"
fi

function i_ps1_print_error()
{
    if [[ $? -ne 0 ]]; then
        printf "$i_ps1_char_error "
    fi
}

function i_ps1_print_git_branch()
{
    local git_branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
    if [ -n "$git_branch" ]; then
        printf "$i_ps1_char_branch$git_branch "
    fi
}

function i_ps1_clock()
{
    local now=$(date +"%T")
    printf $now
}


# Set format of \w, example: ".../_directory_/_directory_/_directory_"
export PROMPT_DIRTRIM=3

export i_ps1_style_1=\
$i_color_fg_red'$(i_ps1_print_error)'\
$i_color_fg_grey$i_ps1_char_clock'$(i_ps1_clock) '\
$i_color_fg_green$i_ps1_char_monitor'\u@\h '\
$i_color_fg_light_blue$i_ps1_char_folder'\w '\
$i_color_fg_reset\
$i_ps1_char_prompt

export i_ps1_style_2=\
$i_color_fg_red'$(i_ps1_print_error)'\
$i_color_fg_green$i_ps1_char_monitor'\u@\h '\
$i_color_fg_light_blue$i_ps1_char_folder'\w '\
$i_color_fg_grey\
'$(i_ps1_print_git_branch)'\
$i_color_fg_reset\
$i_ps1_char_prompt

export i_ps1_style_3=\
$i_color_fg_red'$(i_ps1_print_error)'\
$i_color_fg_grey$i_ps1_char_clock'$(i_ps1_clock) '\
$i_color_fg_green$i_ps1_char_monitor'\u@\h '\
$i_color_fg_light_blue$i_ps1_char_folder'\w '\
$i_color_fg_grey\
'$(i_ps1_print_git_branch)'\
$i_color_fg_reset\
$i_ps1_char_prompt

export jure_stil=\
$i_color_fg_red'$(i_ps1_print_error)'\
$i_color_fg_green$i_ps1_char_monitor' \u: '\
$i_color_fg_light_blue$i_ps1_char_folder' \w '\
$i_color_fg_yellow\
'$(i_ps1_print_git_branch)'\
$i_color_fg_reset\
$i_ps1_char_prompt

function i_ps1_styles_set()
{
    # arg[1] -> default style
    # arg[2] -> style specific for some programs

    if [[ $TERM_PROGRAM == "vscode" ]] || [[ $GIO_LAUNCHED_DESKTOP_FILE == *"clion"* ]]; then
        export PS1=$2
    else
        export PS1=$1
    fi
}
