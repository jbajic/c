#!/bin/bash

alias ga="git add"
alias gb="git branch"
alias gc="git commit"
alias gd="git diff"
alias gl="git log"
alias gr="git reset"
alias gs="git status"
alias gt="git tro"

function i_git_diff_foreach_module()
{
    echo "Entering '.'"
    git --no-pager diff
    echo
    git submodule foreach --recursive "git --no-pager diff && echo"
}

function i_git_pull_master_foreach_module()
{
    echo "Entering '.'"
    git checkout master
    echo
    git submodule foreach --recursive "git checkout master && echo"

    echo "=================================================="
    echo "Entering '.'"
    git pull
    echo
    git submodule foreach --recursive "git pull && echo"
}

function i_git_branch_foreach_module()
{
    echo "Entering '.'"
    git branch -vv
    echo
    git submodule foreach --recursive "git branch -vv && echo"
}

function i_git_log_search()
{
    # param validation
    if [[ ! `git log -n 1 $@ | head -n 1` ]] ;then
        return
    fi

    # filter by file string
    local filter
    # param existed, git log for file if existed
    if [ -n $@ ] && [ -f $@ ]; then
        filter="-- $@"
    fi

    # git command
    local gitlog=(
        git log
        --graph --color=always
        --abbrev=7
        --format='%C(auto)%h %an %C(blue)%s %C(yellow)%cr'
        $@
    )

    # fzf command
    local fzf=(
        fzf
        --ansi --no-sort --reverse --tiebreak=index
        --preview "f() { set -- \$(echo -- \$@ | grep -o '[a-f0-9]\{7\}'); [ \$# -eq 0 ] || git show --color=always \$1 $filter; }; f {}"
        --bind "ctrl-q:abort,ctrl-m:execute:
                    (grep -o '[a-f0-9]\{7\}' | head -1 |
                    xargs -I % sh -c 'git show --color=always % $filter | less -R') << 'FZF-EOF'
                    {}
                    FZF-EOF"
        --preview-window=right:60%
    )

    # piping them
    "${gitlog[@]}" | "${fzf[@]}"
}

function i_git_base()
{
    git rev-parse --show-toplevel
}
