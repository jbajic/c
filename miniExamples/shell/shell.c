#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define NUMBER_OF_CHARACTERS 2
#define TRUE 1

int shell_cd(char **args);
int shell_pwd(char **args);
int shell_help(char **args);
int shell_exit(char **args);

char *build_str[] = {
    "cd",
    "pwd",
    "help",
    "exit"};

int (*buildin_function[])(char **) = {
    shell_cd,
    shell_pwd,
    shell_help,
    shell_exit};

int shell_num_buildins()
{
    return sizeof(build_str) / sizeof(char *);
}

int shell_cd(char **args)
{
    if (args[1] == NULL)
    {
        fprintf(stderr, "shell: Expected argument to cd");
    }
    else
    {
        if (chdir(args[1]) != 0)
        {
            perror("shell");
        }
    }
    return 1;
}

int shell_pwd(char **args)
{
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        printf("Current working dir: %s\n", cwd);
    }
    else
    {
        perror("getcww() error");
    }

    return 1;
}

int shell_help(char **args)
{
    int i;
    printf("Made and maintained by Jure!\n");
    for (i = 0; i < shell_num_buildins(); ++i)
    {
        printf("%s\n", build_str[i]);
    }

    return 1;
}

int shell_exit(char **args)
{
    return 0;
}

char *read_line()
{
    int character;
    int lineSize = NUMBER_OF_CHARACTERS;
    uint32_t index = 0;
    char *line = (char *)malloc(sizeof(char) * NUMBER_OF_CHARACTERS);

    if (line == NULL)
    {
        fprintf(stderr, "Error occured while allocating!");
        exit(1);
    }

    while (TRUE)
    {
        character = getchar();
        if (character == EOF || character == '\n')
        {
            // Exit shell
            line[index] = '\0';
            return line;
        }
        else if (character == 8 && index > 0)
        {
            line[--index] = '\0';
            continue;
        }
        else
        {
            line[index] = character;
        }
        index++;
        if (index > lineSize)
        {
            lineSize += NUMBER_OF_CHARACTERS;
            line = (char *)realloc(line, sizeof(char) * lineSize);
            if (line == NULL)
            {
                fprintf(stderr, "Error occured while allocating!");
                exit(1);
            }
        }
    }
}

#define NUMBER_OF_ARGUMENTS 5
#define COMMAND_DELIMITERS " \t\a\n\r"
char **split_line(char *line)
{
    char **args = (char **)malloc(NUMBER_OF_ARGUMENTS * sizeof(char *));
    char *arg = strtok(line, COMMAND_DELIMITERS);
    int32_t argumentCounter = 0;
    int32_t linePosition = 0;
    if (args == NULL)
    {
        fprintf(stderr, "Error while allocating!\n");
        exit(1);
    }

    while (arg != NULL)
    {
        args[argumentCounter] = (char *)malloc(strlen(arg) * sizeof(char));
        strcpy(args[argumentCounter], arg);
        argumentCounter++;
        arg = strtok(NULL, COMMAND_DELIMITERS);
    }

    return args;
}

int execute_command(char **args)
{
    int i;
    if (args[0] == NULL)
    {
        return 1;
    }
    for (i = 0; i < shell_num_buildins(); ++i)
    {
        if (strcmp(args[0], build_str[i]) == 0)
        {
            return (*buildin_function[i])(args);
        }
    }
    return execute_process(args);
}

int execute_process(char **args)
{
    pid_t pid, wpid;
    int status;

    pid = fork();
    if (pid == 0)
    {
        if (execvp(args[0], args) == -1)
        {
            perror("shell");
        }
        exit(2);
    }
    else if (pid < 0)
    {
        perror("Error forking!");
    }
    else
    {
        do
        {
            wpid = waitpid(pid, &status, WUNTRACED);

        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    return 1;
}

void init_loop()
{
    int status;
    // char **args;
    char *line;
    char **args;

    do
    {
        printf("> ");
        line = read_line();
        printf("%s\n", line);
        args = split_line(line);
        status = execute_command(args);

        free(line);
        free(args);
    } while (status);
}

int main(int argc, char *argv[])
{
    printf("%d\n", argc);
    // printf("%s\n", argv[1]);
    init_loop();
    return 0;
}