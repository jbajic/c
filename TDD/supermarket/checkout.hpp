#ifndef C_CHECKOUT_H
#define C_CHECKOUT_H

#include <string>
#include <map>

class Checkout {
public:
    Checkout();

    virtual  ~Checkout();

    void addItemPrice(std::string item, int price);

    void addItem(std::string item);

    int calculateTotal();

    void addDiscount(std::string item, int numberOfItems, int discountPrice);

    void calculateItem(std::string item, int itemCount);

private:
    struct Discount {
        int numOfItems;
        int discountPrice;
    };
    std::map<std::string, int> prices;
    std::map <std::string, Discount> discounts;
    std::map<std::string, int> items;

    int total;
};


#endif //C_CHECKOUT_H
