#include <gtest/gtest.h>
#include <iostream>
#include "checkout.hpp"

using namespace std;

class CheckoutTest : public ::testing::Test {
public:

protected:
    Checkout checkOut;
};


TEST_F(CheckoutTest, CanCalculateTotal) {
    checkOut.addItemPrice("a", 3);
    checkOut.addItem("a");
    int result = checkOut.calculateTotal();
    ASSERT_EQ(3, result);
}

TEST_F(CheckoutTest, CanGetTotalForMultipleItems) {
    checkOut.addItemPrice("a", 1);
    checkOut.addItemPrice("b", 2);
    checkOut.addItem("a");
    checkOut.addItem("b");
    int result = checkOut.calculateTotal();
    ASSERT_EQ(3, result);
}

TEST_F(CheckoutTest, CanAddDiscount) {
    checkOut.addDiscount("a", 3, 2);
}

TEST_F(CheckoutTest, CanCalculateTotalWithDiscount) {
    checkOut.addItemPrice("a", 1);
    checkOut.addDiscount("a", 3, 2);
    checkOut.addItem("a");
    checkOut.addItem("a");
    checkOut.addItem("a");
    int total = checkOut.calculateTotal();
    ASSERT_EQ(2, total);
}