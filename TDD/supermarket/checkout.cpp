#include "checkout.hpp"

Checkout::Checkout() : total{0} {}

Checkout::~Checkout() {}

void Checkout::addItemPrice(std::string item, int price) {
    prices[item] = price;
}

void Checkout::addItem(std::string item) {
    items[item]++;
}

int Checkout::calculateTotal() {
    total = 0;
    for (std::map<std::string, int>::iterator it = items.begin(); it != items.end(); it++) {
        std::string item = it->first;
        int itemCount = it->second;

        std::map<std::string, Discount>::iterator discountIter;
        discountIter = discounts.find(item);
        if (discountIter != discounts.end()) {
            Discount discount = discountIter->second;
            if (itemCount >= discount.numOfItems) {
                int numberOfDiscounts = itemCount / discount.numOfItems;
                total += numberOfDiscounts * discount.discountPrice;
                int remainingItems = itemCount % discount.numOfItems;
                total += remainingItems * prices[item];
            } else {
                total += itemCount / prices[item];
            }
        } else{
            total += itemCount * prices[item];
        }
    }
    return total;
}

void Checkout::calculateItem(std::string item, int itemCount) {

}

void Checkout::addDiscount(std::string item, int numberOfItems, int discountPrice) {
    Discount discount;
    discount.numOfItems = numberOfItems;
    discount.discountPrice = discountPrice;
    discounts[item] = discount;
}
