#include <gtest/gtest.h>
#include "kata.hpp"
#include <string>

using namespace std;

TEST(FizzBuzzTest, Returns1With1Passed) {
    string result = fizzBuzz(1);
    ASSERT_STREQ("1", result.c_str());
}

void checkFizzBuzz(int value, string expectedResultString) {
    string result = fizzBuzz(value);
    ASSERT_STREQ(expectedResultString.c_str(), result.c_str());
}

TEST(FizzBuzzTest, Return1With1PassedIn) {
    checkFizzBuzz(1, "1");
}

TEST(FizzBuzzTest, Return2With2PassedIn) {
    checkFizzBuzz(2, "2");
}

TEST(FizzBuzzTest, ReturnFizzWhen3Passed) {
    checkFizzBuzz(3, "Fizz");
}

TEST(FizzBuzzTest, ReturnBuzzWhen5Passed) {
    checkFizzBuzz(5, "Buzz");
}

TEST(FizzBuzzTest, ReturnFizzWhen6Passed) {
    checkFizzBuzz(6, "Fizz");
}

TEST(FizzBuzzTest, ReturnBuzzWhen10Passed) {
    checkFizzBuzz(10, "Buzz");
}

TEST(FizzBuzzTest, ReturnFizzBuzzWhen1Passed) {
    checkFizzBuzz(15, "FizzBuzz");
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}