#ifndef C_KATA_HPP
#define C_KATA_HPP

#include <string>

using namespace std;

bool isDividable(int number, int divider) {
    return number % divider == 0;
}

string fizzBuzz(int num) {
    if (isDividable(num, 3) && isDividable(num, 5)) {
        return "FizzBuzz";
    } else if (isDividable(num, 3)) {
        return "Fizz";
    } else if (isDividable(num, 5)) {
        return "Buzz";
    } else return to_string(num);
}

#endif //C_KATA_HPP
