#include <iostream>
#include <string>

using namespace std;

void permute(string, unsigned int, unsigned int);
void change(string&, int, int);

int main(int argc, char* argv[]) {
    string a = "ABC";
//    cout << a.size() << endl;
    permute(a, 0, a.size() - 1);

    return 0;
}

void permute(string s, unsigned int start, unsigned int end) {
    if(start == end) {
        cout << s << endl;
    }
    for (unsigned int i = start; i <= end; i++) {
        change(s, start, i);
        permute(s, start + 1, end);
        change(s, start, i);
    }
}

void change(string& s, int indexOne, int indexTwo) {
    char temp = s[indexOne];
    s[indexOne] = s[indexTwo];
    s[indexTwo] = temp;
}