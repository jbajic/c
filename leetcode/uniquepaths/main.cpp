#include <iostream>

using namespace std;

struct Node
{
    int x, y;
    Node *left;
    Node *right;
    Node(int x, int y) : x{x}, y{y}, left{nullptr}, right{nullptr} {}
};

class BinaryTree
{
public:
    Node *root;

    BinaryTree(int x, int y) : root{new Node(x, y)} {}

    Node *insertOn(int x, int y, Node *leaf)
    {
        if (leaf->left == nullptr)
        {
            leaf->left = new Node(x, y);
        }
        else if (leaf->right == nullptr)
        {
            leaf->right = new Node(x, y);
        }
    }

};

void printBT(const std::string& prefix, const Node* node, bool isLeft)
{
    if( node != nullptr )
    {
        std::cout << prefix;

        std::cout << (isLeft ? "├──" : "└──" );

        // print the value of the node
        std::cout << node->x << "," << node->y << std::endl;

        // enter the next tree level - left and right branch
        printBT( prefix + (isLeft ? "│   " : "    "), node->left, true);
        printBT( prefix + (isLeft ? "│   " : "    "), node->right, false);
    }
    }

void printBT(const Node* node)
{
    printBT("", node, false);    
}

unsigned int getNumberOfLeaves(Node* node)
{
    if(node == nullptr) return 0;
    if (node->left == nullptr && node->right == nullptr)
    {
        return 1;
    }
    else {
        return getNumberOfLeaves(node->left) + getNumberOfLeaves(node->right);
    }
}

void uniquePaths(Node *root, int m, int n)
{
    if (root->x < m)
    {
        root->left = new Node(root->x + 1, root->y);
        uniquePaths(root->left, m, n);
    }
    if (root->y < n)
    {
        root->right = new Node(root->x, root->y +1);
        uniquePaths(root->right, m, n);
    }
    return;
}

int main()
{
    int width = 23;
    int height = 12;
    BinaryTree bt(0, 0);
    uniquePaths(bt.root, width, height);
    cout << "NumberOFPAths" << endl;
    cout << "Number of paths: " << getNumberOfLeaves(bt.root) << endl;
    // printBT(bt.root);
    return 0;
}
