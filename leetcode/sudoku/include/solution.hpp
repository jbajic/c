#include <vector>
#include <set>
#include <map>
#include <iostream>

using namespace std;

class Solution
{
public:
    bool isValidSudoku(vector<vector<char>> &board);

    bool isValidRow(vector<char> &row);

    bool isValidColumn(vector<vector<char>> &table, int column);

    bool areGridsValid(vector<vector<char>> &table);
};