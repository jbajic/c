#include "solution.hpp"

// Runtime: 20 ms, faster than 32.85% of C++ online submissions for Valid Sudoku.
// Memory Usage: 12 MB, less than 20.51% of C++ online submissions for Valid Sudoku.


bool Solution::isValidSudoku(vector<vector<char>> &board)
{
    for (int i = 0; i < board.size(); ++i)
    {
        if (!isValidRow(board[i]) || !isValidColumn(board, i))
        {
            cout << "Row column:" << i << endl;
            return false;
        }
    }
    cout << "LADIDA" << endl;
    return areGridsValid(board);
}

bool Solution::isValidRow(vector<char> &row)
{
    set<char> uniques;
    for (char element : row)
    {
        if (element != '.')
        {
            auto ret = uniques.insert(element);
            if (!ret.second)
            {
                return false;
            }
        }
    }
    return true;
}

bool Solution::isValidColumn(vector<vector<char>> &table, int column)
{
    set<char> uniques;
    for (auto row : table)
    {
        char element = row[column];
        if (element != '.')
        {
            auto ret = uniques.insert(element);
            if (!ret.second)
            {
                return false;
            }
        }
    }
    return true;
}

bool Solution::areGridsValid(vector<vector<char>> &table)
{
    map<pair<int, int>, set<char>> grids;
    for (int i = 0; i < table.size(); ++i)
    {
        for (int j = 0; j < table[i].size(); ++j)
        {
            if (table[i][j] != '.')
            {
                pair<int, int> key{i / 3, j / 3};
                if (grids.find(key) == grids.end())
                {
                    grids.insert({key, set<char>()});
                }
                auto ret = grids.at(key).insert(table[i][j]);
                if (!ret.second)
                {
                    cout << "GRID:" << i << "  " << j << endl;
                    return false;
                }
            }
        }
    }
    return true;
}