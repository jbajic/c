#include "solution.hpp"

// Runtime: 16 ms, faster than 63.45% of C++ online submissions for Valid Sudoku.
// Memory Usage: 11.9 MB, less than 20.51% of C++ online submissions for Valid Sudoku.

bool Solution::isValidSudoku(vector<vector<char>> &board)
{
    for (int i = 0; i < board.size(); ++i)
    {
        if (!isValidRow(board[i]) || !isValidColumn(board, i))
        {
            cout << "Row column:" << i << endl;
            return false;
        }
    }

    return areGridsValid(board);
}

bool Solution::isValidRow(vector<char> &row)
{
    set<char> uniques;
    for (char element : row)
    {
        if (element != '.')
        {
            auto ret = uniques.insert(element);
            if (!ret.second)
            {
                return false;
            }
        }
    }
    return true;
}

bool Solution::isValidColumn(vector<vector<char>> &table, int column)
{
    set<char> uniques;
    for (auto row : table)
    {
        char element = row[column];
        if (element != '.')
        {
            auto ret = uniques.insert(element);
            if (!ret.second)
            {
                return false;
            }
        }
    }
    return true;
}

bool Solution::areGridsValid(vector<vector<char>> &board)
{
    for (int i = 0; i < board.size(); i += 3)
    {
        for (int j = 0; j < board[i].size(); j += 3)
        {
            //check subgrid
            set<int> uniques;
            for (int k = i; k < (i + 3); ++k)
            {
                for (int l = j; l < (j + 3); ++l)
                {
                    if (board[l][k] != '.')
                    {
                        auto ret = uniques.insert(board[l][k]);
                        if (!ret.second)
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }
    return true;
}