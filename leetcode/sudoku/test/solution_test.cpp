#include <gtest/gtest.h>
#include "solution.hpp"

class SolutionTest : public ::testing::Test
{
public:
protected:
    Solution solution;
};

TEST_F(SolutionTest, CheckIfRowIsValid)
{
    vector<char> test1{'1', '.', '2', '1'};
    vector<char> test2{'1', '.', '2', '3'};
    EXPECT_FALSE(solution.isValidRow(test1));
    EXPECT_TRUE(solution.isValidRow(test2));
}

TEST_F(SolutionTest, CheckIfColumnIsValid)
{
    vector<vector<char>> test1{
        {'1', '2', '3','4'},
        {'1', '1', '2','3'},
        {'4', '3', '1','1'},
        {'3', '4', '2','1'},
    };
    EXPECT_FALSE(solution.isValidColumn(test1, 0));
    EXPECT_TRUE(solution.isValidColumn(test1, 1));
}

TEST_F(SolutionTest, CheckIfSudokuIsValid)
{
    vector<vector<char>> test1{
        {'5','3','.','.','7','.','.','.','.'},
        {'6','.','.','1','9','5','.','.','.'},
        {'.','9','8','.','.','.','.','6','.'},
        {'8','.','.','.','6','.','.','.','3'},
        {'4','.','.','8','.','3','.','.','1'},
        {'7','.','.','.','2','.','.','.','6'},
        {'.','6','.','.','.','.','2','8','.'},
        {'.','.','.','4','1','9','.','.','5'},
        {'.','.','.','.','8','.','.','7','9'}
    };
    vector<vector<char>> test2{
        {'8','3','.','.','7','.','.','.','.'},
        {'6','.','.','1','9','5','.','.','.'},
        {'.','9','8','.','.','.','.','6','.'},
        {'8','.','.','.','6','.','.','.','3'},
        {'4','.','.','8','.','3','.','.','1'},
        {'7','.','.','.','2','.','.','.','6'},
        {'.','6','.','.','.','.','2','8','.'},
        {'.','.','.','4','1','9','.','.','5'},
        {'.','.','.','.','8','.','.','7','9'}
    };
    EXPECT_TRUE(solution.isValidSudoku(test1));
    // EXPECT_FALSE(solution.isValidSudoku(test2));
}