#include <string>
#include <vector>

using namespace std;

class Solution {
 public:
//   string convert(string s, int numRows) {
//     string rows{""};
//     string zigZag{""};
//     string newString{""};

//     int counterRows{0};
//     int counterZigZag{0};
//     int numOfEntities =
//         ceil(static_cast<float>(s.size()) / (numRows + numRows - 2));

//     for (char& character : s) {
//       if (counterRows < numRows) {
//         rows.push_back(character);
//         counterRows++;
//       } else if (counterZigZag < numRows - 2) {
//         zigZag.push_back(character);
//         counterZigZag++;
//       } else {
//         rows.push_back(character);
//         counterRows = 1;
//         counterZigZag = 0;
//       }
//     }
//     string::iterator rowsIter = rows.begin();
//     string::iterator zigZagIter = zigZag.begin();
//     int zigZagLength = numRows - 2;
//     if (numRows < 3) zigZagLength = numRows;
//     for (int i = 0; i < numRows; ++i) {
//       for (int j = 0; j < numOfEntities; ++j) {
//         int rowsElementIndex = i + j * numRows;
//         if (rows.size() > rowsElementIndex)
//           newString.push_back(rows.at(rowsElementIndex));
//         if (i != 0 && i != (numRows - 1)) {
//           int zigZagElementIndex = j * zigZagLength + zigZagLength - i;
//           if (zigZagElementIndex < zigZag.size())
//             newString.push_back(zigZag.at(zigZagElementIndex));
//         }
//         // cout << newString << endl;
//       }
//       // cout << newString << endl;
//     }

//     return newString;
//   }

  string convert(string s, int numRows) {
    string newString{""};
    int numberOfZigZagElements{numRows - 2};
    if (numberOfZigZagElements < 0) numberOfZigZagElements = 0;

    for (int i = 0; i < numRows; ++i) {
      for (int j = 0; j < s.size(); j += (numRows + numberOfZigZagElements)) {
        int rowIndex = j + i;
        if (rowIndex < s.size()) {
          newString.push_back(s[rowIndex]);
        }
        if (i != 0 && i != (numRows - 1) && numberOfZigZagElements != 0) {
          int zigZagindex = (j + numRows) + numberOfZigZagElements - i;
          if (zigZagindex < s.size()) {
            newString.push_back(s[zigZagindex]);
          }
        }
      }
    }
    return newString;
  }
};

int main() {
  Solution sol;
  string str{"PAYPALISHIRING"};
  cout << str << endl;
  cout << sol.convert(str, 3) << endl;
  return 0;
}