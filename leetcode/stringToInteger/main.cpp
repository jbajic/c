#include <iostream>
#include <string>
#include <bits/stdc++.h> 

using namespace std;

class Solution {
public:
    bool isOverflow(int num1, int num2) {
        if(num1 > (INT_MAX - num2) / 10) {
            cout << "Overflow occured!" << endl;
            return true;
        }
        return false;
    }

    int myAtoi(string str) {
        int num{0};
        int sign{1};
        int i = str.find_first_not_of(' ');
        if (i == string::npos) i = 0;
        if(str[i] == '-' || str[i] == '+') {
            str[i] == '+'? sign = 1: sign = -1;
            i++;
        }
        for(; i < str.size(); ++i) {
            if(str[i] > 47 && str[i] < 58) {
                int element = str[i] - 48;
                if(isOverflow(num, element)) {
                    return sign == 1? INT_MAX:INT_MIN;
                }
                num = num * 10 + element;
            } else  {
                return num * sign;
            }
        }
        return num * sign;
    }
};

int main() {
    Solution sol;
    cout << sol.myAtoi("1234") << endl;
    cout << sol.myAtoi("8932") << endl;
    cout << sol.myAtoi("8932") << endl;
    cout << sol.myAtoi("-21") << endl;
    cout << sol.myAtoi("  8932") << endl;
    cout << sol.myAtoi("-12") << endl;
    cout << sol.myAtoi("0") << endl;
    cout << sol.myAtoi("-0") << endl;
    cout << sol.myAtoi("  -10") << endl;
    cout << sol.myAtoi("ladida  -10") << endl;
    cout << sol.myAtoi("  -10 dadads") << endl;
    cout << sol.myAtoi("  -31 sasa") << endl;
    cout << sol.myAtoi("  -91283472332 sasa") << endl;
    cout << sol.myAtoi("  -91283472332 sasa") << endl;
    cout << sol.myAtoi("  941283472332 sasa") << endl;
    cout << sol.myAtoi("  +1 sasa") << endl;
    cout << sol.myAtoi("  +-2 sasa") << endl;
    cout << sol.myAtoi("dfjslfjk") << endl;
    // cout << "MIN" << INT_MIN << endl;
    // cout << "MAX" <<  INT_MAX << endl;
    return 0;
}