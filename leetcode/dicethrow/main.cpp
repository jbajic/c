#include <iostream>
#include <string>
#include <vector>

using namespace std;

int findWays(int faces, int dices, int y    ) {
    vector<vector<int>> dp(dices+1, vector<int>(target + 1, 0));

    // Table entries for one dice
    for(int j = 1; j <= faces && j<= target; j++) {
        dp[1][j] = 1;
    }

    for(int i = 2; i <= dices; i++) {
        for(int j = 1; j <= target; j++) {
            for(int k = 1; k <= faces && k <= j; k++) {
                dp[i][j] += dp[i-1][j-k];
            }
        }
    }

    // for(int i = 0; i <= dices; i++) {
    //     for(int j = 0; j <= target; j++) {
    //         cout << dp[i][j] << " ";
    //     }
    //     cout << endl;
    // }

    return dp[dices][target];
}

class Solution {
public:
    int numRollsToTarget(int d, int f, int target) {
        vector<vector<int>> dp(d+1, vector<int>(target+1, 0));
        
        for(int i = 1; i <= f && i <= target; i++) {
            dp[1][i] = 1;
        }
        
        for(int i = 2; i <= d; i++) {
            for(int j = 1; j <= target; j++) {
                for(int k = 1; k <= j && k <= f; k++) {
                    dp[i][j] += dp[i-1][j-k];
                }
            }
        }

        return dp[d][target];
    }
};

int main() {
//   cout << "Solution is: " << findWays(4, 2, 1) << endl;
//   cout << findWays(2, 2, 3) << endl;
  cout << findWays(6, 3, 8) << endl;
  return 0;
}