#ifndef C_SOLUTION_HPP
#define C_SOLUTION_HPP

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s);
};

#endif //C_SOLUTION_HPP
