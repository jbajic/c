#include <gtest/gtest.h>
#include "solution.hpp"

TEST(Solution, TestLongestLengthOfSubstring) {
    std::string input = "abcabcbb";
    std::string input2 = "bbbbb";
    std::string input3 = "pwwkew";
    std::string input4 = "aab";
    std::string input5 = "dvdf";
    Solution sol;

    EXPECT_EQ(3, sol.lengthOfLongestSubstring(input));
    EXPECT_EQ(1, sol.lengthOfLongestSubstring(input2));
    EXPECT_EQ(3, sol.lengthOfLongestSubstring(input3));
    EXPECT_EQ(2, sol.lengthOfLongestSubstring(input4));
    EXPECT_EQ(3, sol.lengthOfLongestSubstring(input5));
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}