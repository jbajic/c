#include <iostream>

using namespace std;

//  Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(const ListNode& other) {
        cout << "Copy constructor!" << endl;
        val = other.val;
        next = nullptr;
    }
    ListNode& operator=(ListNode& other) {
        cout << "Copy operator!" << endl;
        val = other.val;
        next = nullptr;
        return *this;
    }
    ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
public:

    ListNode* mergeTwoListsWithoudChanging(ListNode* l1, ListNode* l2) {
        ListNode dummyHead(0);
        ListNode* tail = &dummyHead;
        while(l1 != NULL && l2 != NULL) {
            if(l1->val <= l2->val) {
                tail->next = new ListNode(l1->val);
                l1 = l1->next;
            } else {
                tail->next = new ListNode(l2->val);
                l2 = l2->next;
            }
            tail = tail->next;
        }
        while(l1 != NULL) {
            tail->next = new ListNode(l1->val);
            l1 = l1->next;
        }

        while(l2 != NULL) {
            tail->next = new ListNode(l2->val);
            l2 = l2->next;
        }
        return dummyHead.next;
    }

    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        ListNode dummyHead(0);
        ListNode* tail = &dummyHead;
        while(l1 != NULL && l2 != NULL) {
            if(l1->val <= l2->val) {
                tail->next = l1;
                l1 = l1->next;
            } else {
                tail->next = l2;
                l2 = l2->next;
            }
            tail = tail->next;
        }
        if(l1 == NULL) {
            tail->next = l1;
        }

        if(l2 == NULL) {
            tail->next = l2;
        }
        return dummyHead.next;
    }

    ListNode* mergeTwoListsBest(ListNode* l1, ListNode* l2) {
        ListNode dummyHead(0);
        ListNode* tail = &dummyHead;
        while(true) {
            if(l1 == NULL) {
                tail->next = l2;
                break;
            }
            if(l2 == NULL) {
                tail->next = l1;
                break;
            }
            if(l1->val <= l2->val) {
                tail->next = l1;
                l1 = l1->next;
            } else {
                tail->next = l2;
                l2 = l2->next;
            }
            tail = tail->next;
        }
        return dummyHead.next;;
    }

    void printList(ListNode* head) {
        while(head != NULL) {
            cout << head->val << ", ";
            head = head->next;
        }
        cout << endl;
    }

};

int main() {
    Solution sol;
    ListNode* l1{new ListNode(1)};
    l1->next = new ListNode(3);
    l1->next->next = new ListNode(3);

    ListNode* l2{new ListNode(0)};
    l2->next = new ListNode(2);
    l2->next->next = new ListNode(5);
    sol.printList(l1);
    sol.printList(l2);
    ListNode* merged = sol.mergeTwoListsWithoudChanging(l1, l2);
    sol.printList(l1);
    sol.printList(l2);
    sol.printList(merged);
    return 0;
}