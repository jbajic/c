#include <gtest/gtest.h>
#include "list_node.hpp"

TEST(AddTwoNumbers, NumberAddition) {
    ListNode* node1 = new ListNode(2);
    node1->next = new ListNode(4);
    node1->next->next = new ListNode(3);
    
    ListNode* node2 = new ListNode(5);
    node2->next = new ListNode(6);
    node2->next->next = new ListNode(4);

    ListNode* result_mock = new ListNode(7);
    result_mock->next = new ListNode(0);
    result_mock->next->next = new ListNode(8);
    
    Solution solution;
    ListNode* result = solution.addTwoNumbers(node1, node2);
    EXPECT_EQ(result_mock->val, result->val);
    EXPECT_EQ(result_mock->next->val, result->next->val);
    EXPECT_EQ(result_mock->next->next->val, result->next->next->val);
}

TEST(AddTwoNumbers, NumberOneDigit) {
    ListNode* node1 = new ListNode(5);
    
    ListNode* node2 = new ListNode(5);

    ListNode* result_mock = new ListNode(0);
    result_mock->next = new ListNode(1);
    
    Solution solution;
    ListNode* result = solution.addTwoNumbers(node1, node2);
    EXPECT_EQ(result_mock->val, result->val);
    EXPECT_EQ(result_mock->next->val, result->next->val);
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}