#include <memory>
#include <iostream>

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
    {
        ListNode *result = new ListNode(0);
        ListNode *node = result;
        int pass = 0;

        while (true)
        {
            int temp{0};
            if (l1 != NULL)
            {
                temp += l1->val;
            }
            if (l2 != NULL)
            {
                temp += l2->val;
            }
            temp += pass;
            if (temp > 9)
            {
                pass = 1;
                temp = temp % 10;
            }
            else
            {
                pass = 0;
            }
            node->val = temp;
            std::cout << "Num: " << node->val << std::endl;
            if (l1 != NULL)
                l1 = l1->next;
            if (l2 != NULL)
                l2 = l2->next;
            if (l1 != NULL || l2 != NULL || pass != 0)
            {
                node->next = new ListNode(0);
                node = node->next;
            }
            else
            {
                break;
            }
        }

        return result;
    }
};
