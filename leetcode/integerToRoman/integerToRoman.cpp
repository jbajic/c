#include <iostream>
#include <string>
#include <vector>

using namespace std;

class IntegerToRomanConverter
{
private:
    vector<string> romanNumbers{"I", "V", "X", "L", "C", "D", "M"};

public:
    string convertDigit(int num, int powerOfTen)
    {
        string temp{""};

        if (num == 0)
        {
            return temp;
        }
        else if (powerOfTen > 2)
        {
            for (int i = 0; i < num; ++i)
            {
                temp += romanNumbers[6];
            }
        }
        else if (num < 4)
        {
            for (int i = 0; i < num; ++i)
            {
                temp += romanNumbers[powerOfTen * 2];
            }
        }
        else if (num == 4)
        {
            temp = romanNumbers[powerOfTen * 2] + romanNumbers[powerOfTen * 2 + 1];
        }
        else if (num == 5)
        {
            temp = romanNumbers[powerOfTen * 2 + 1];
        }
        else if (num < 9)
        {
            temp = romanNumbers[powerOfTen * 2 + 1];
            for (int i = 0; i < num - 5; ++i)
            {
                temp += romanNumbers[powerOfTen * 2];
            }
        }
        else
        {
            temp = romanNumbers[powerOfTen * 2] + romanNumbers[powerOfTen * 2 + 2];
        }
        cout << temp << endl;
        return temp;
    }
};

int main()
{
    IntegerToRomanConverter converter;
    int num;
    string romaNumber{""};
    cout << "Enter a number" << endl;
    cin >> num;
    cout << "The value you entered is " << num << endl;
    for (int i = 0; num > 0; i++)
    {
        int currentDigit = num % 10;
        if (currentDigit != 0)
        {
            romaNumber.insert(0, converter.convertDigit(currentDigit, i));
        }
        num /= 10;
    }
    cout << romaNumber << endl;
    return 0;
}