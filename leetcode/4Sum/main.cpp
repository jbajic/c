// Given an array nums of n integers and an integer target, are there elements a, b, c,
//  and d in nums such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.
#include <iostream>
#include <vector>
#include <functional>
#include <numeric>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Solution {
private:
    static const int setNum{4};
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>> result;
        vector<int> combination;
        int size{static_cast<int>(nums.size())};
        for(int i = 0; i < size - (setNum-1); ++i) {
            for(int j = i + 1; j < size ; ++j) {
                for(int z = j + 1; z < size; ++z) {
                    int partialSum = nums[i] + nums[j] + nums[z];
                    for(int k = z + 1; k < size; ++k) {
                        if (partialSum + nums[k] == target) {
                            if(partialSum + nums[k] == target) {
                                combination.push_back(nums[i]);
                                combination.push_back(nums[j]);
                                combination.push_back(nums[z]);
                                combination.push_back(nums[k]);
                                bool alredyExists = false;
                                for(auto combRes: result) {
                                    vector<int> combinationCopy = combination;
                                    for(auto it = combRes.begin(); it != combRes.end(); it++) {
                                        auto foundIt = find(combinationCopy.begin(), combinationCopy.end(), *it);
                                        if(foundIt != combinationCopy.end()) {
                                            combRes.erase(it--);
                                            combinationCopy.erase(foundIt);
                                        }
                                    }
                                    if(combRes.empty()) {
                                        alredyExists = true;
                                        break;
                                    }
                                }
                                if(!alredyExists) {
                                    result.emplace_back(combination);
                                }
                            }
                            combination.clear();
                        }
                    }
                }
            }
        }
        return result;
    }
};

int main() {
    vector<int> nums1{-499,-486,-479,-462,-456,-430,-415,-413,-399,-381,-353,-349,-342,-337,-336,-331,-330,-322,-315,-280,-271,-265,-249,-231,-226,-219,-216,-208,-206,-204,-188,-159,-144,-139,-123,-115,-99,-89,-80,-74,-61,-22,-22,-8,-5,4,43,65,82,86,95,101,103,123,149,152,162,165,168,183,204,209,209,220,235,243,243,244,248,253,260,273,281,284,288,290,346,378,382,384,407,411,423,432,433,445,470,476,497};
    int tag1 = 3032;
    vector<int> nums2{0,0,0,0};
    int tag2 = 0;
    vector<int> nums3{1,0,-1,0,-2,2};
    int tag3 = 0;
    vector<int> nums4{-497,-473,-465,-462,-450,-445,-411,-398,-398,-392,-382,-376,-361,-359,-353,-347,-329,-328,-317,-307,-273,-230,-228,-227,-217,-199,-190,-175,-155,-151,-122,-102,-97,-96,-95,-87,-85,-84,-73,-71,-51,-50,-39,-24,-19,-1,-1,7,22,25,27,37,40,43,45,51,72,91,97,108,119,121,122,123,127,156,166,171,175,180,203,211,217,218,224,231,245,293,297,299,300,318,326,336,353,358,376,391,405,423,445,451,459,464,471,486,487,488};
    int tag4 = 2251;
    Solution sol;
    vector<vector<int>> result = sol.fourSum(nums4, tag4);
    for(auto set:result) {
        cout << "[";
        for(int element:set) {
            cout << element << ", ";
        }
        cout << "]" << endl;
    }
    return 0;
}
