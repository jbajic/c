#include <iostream>
#include <memory>
using namespace std;

class Test{
    public:
    Test() {
        cout << "created" << endl;
    }

    void greet() {
        cout << "Hello" << endl;
    }

    ~Test() {
        cout << "destroyed" << endl;
    }
};

class Temp {
    private:
        unique_ptr<Test[]> pTest;

    public:
    Temp(): pTest(new Test[2]) {

    }
};

int main() {
    // unique_ptr<int> pTest(new int);
    // *pTest = 7;
    //Auto pointer is deprecated, and doesnt work with arrays
    unique_ptr<Test> pTest(new Test);
    unique_ptr<Test[]> pTests(new Test[2]);

    pTest->greet();
    pTests[1].greet();

    cout << "Finished" << endl;

    Temp temp;
    return 0;
}