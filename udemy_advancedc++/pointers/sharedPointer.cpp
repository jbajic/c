#include <iostream>
#include <memory>
using namespace std;

class Test{
    public:
    Test() {
        cout << "created" << endl;
    }

    void greet() {
        cout << "Hello" << endl;
    }

    ~Test() {
        cout << "destroyed" << endl;
    }
};


int main() {
    shared_ptr<Test> pTest2(nullptr);
    // shared_ptr<Test> sTest1(new Test);
    {
        shared_ptr<Test> sTest1 = make_shared<Test>();
        pTest2 = sTest1;
    }


    cout << "Hello World!" << endl;
    return 0;
}