#include <iostream>
#include <functional>

using namespace std;
using namespace placeholders;

int add(int a, int b, int c) {
    cout << a << ", " << b << ", " << c << endl;
    return a + b + c;
}

int run(function<int(int, int)> funct) {
    return funct(7, 3);
}

class Test {
    public:
        int add(int a, int b, int c) {
            cout << a << ", " << b << ", " << c << endl;
            return a + b + c;
        }
};

int main() {
    auto calc = bind(add, 3, 4, 5);
    auto calc2 = bind(add, _1, _2, _3);
    auto calc3 = bind(calc2, _3, _2, _1);
    auto calc4 = bind(calc2, _1, _2, 100);

    Test test;
    auto calc5 = bind(&Test::add, test, _1, _2, 500);

    //Bind function to varible
    cout << calc() << endl;
    cout << calc2(1, 2, 3) << endl;
    cout << calc3(1, 2, 3) << endl;

    cout << run(calc4) << endl;
    cout << run(calc5) << endl;
    return 0;
}