#include <iostream>
using namespace std;

class Test {

};

template<typename T>
//Reference collapsing rule
void call(T&& arg) {
    // check(static_cast<T>(arg)); static_Cast =~ forward
    check(forward<T>(arg));
}

void check(Test& test) {
    cout << "LValue" << endl;
}

void check(Test&& test) {
    cout << "RValue" << endl;
}

int main() {
    Test test;
    // RValue reference
    auto&& T = Test();

    // auto&& t = test;

    //Both are lValue
    call(Test());
    call(test);
    //But when static cast is added that is fixed

    return 0;
}