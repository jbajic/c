#include <iostream>
#include <vector>
#include <memory.h>
using namespace std;

class Test {
    private:
        static const int SIZE = 100;

        int *_pBuffer;
    public:
        Test() {
            cout << "constructor" << endl;
            // _pBuffer = new int[SIZE];
            // memset(_pBuffer, 0, sizeof(int) * SIZE);
            _pBuffer = new int[SIZE]{};
        }

        Test(int i) {
            cout << "parameterized constructor" << endl;
            _pBuffer = new int[SIZE]{};

            for (int i = 0; i < SIZE; ++i) {
                _pBuffer[i] = 7 * i;
            }
        }

        Test(const Test& other) {
            cout << "copy consturcotr" << endl;
            _pBuffer = new int[SIZE]{};
            memcpy(_pBuffer, other._pBuffer, sizeof(int) * SIZE);
        }

        Test operator=(const Test& other) {
            cout << "assigment" << endl;
            _pBuffer = new int[SIZE]{};
            memcpy(_pBuffer, other._pBuffer, sizeof(int) * SIZE);
            return *this;
        }

        ~Test() {
            cout << "destructor" << endl;
            delete[] _pBuffer;
        }

        friend ostream &operator<<(ostream &out, const Test &test);
};

ostream &operator<<(ostream& out, const Test& test) {
    out << "Hello from test";
    return out;
}

Test getTest() {
    return Test();
}

int main() {

    Test test1 = getTest();

    //Try without this
    vector<Test> vec;
    vec.push_back(Test());

    Test &rTest1 = test1;

    // Not working
    // Test &rTest2 = getTest();
    const Test &rTest2 = getTest();

    Test test2(Test(1));

    return 0;
}