#include <iostream>

using namespace std;

template<class T>
class ring {
    private:
        int size;
        int innerPointer = 0;
        T* buffer;

    public:
        class iterator;

        ring(int s): buffer(new T[s]), size(s), innerPointer(0) {}

        ring(const ring& other) {
            buffer = new T[other.getSize()];
            for(int i = 0; i < other.getSize(); ++i) {
                buffer[i] = other.buffer[i];
            }
            size = other.getSize();
        }

        T& get(int index) const {
            return buffer[index];
        }

        int getSize() const {
            return size;
        }

        void add(const T element) {
            if (innerPointer == size) {
                innerPointer = 0;
            }
            buffer[innerPointer++] = element;
        }

        iterator begin() {
            return iterator(0, *this);
        }

        iterator end() {
            return iterator(size, *this);
        }

        ~ring() {
            delete[] buffer;
        }
};

template<class T>
class ring<T>::iterator {
    public:
    void print() {
        cout << "Hello from the other side!" << endl;
    }

    iterator(int _position, ring& _ringBuffer): position(_position), ringBuffer(_ringBuffer) {}

    iterator& operator++() {
        position++;
        return *this;
    }

    iterator& operator++(int) {
        position++;
        return *this;
    }

    bool operator!=(const iterator& other) const {
        return position != other.position;
    }

    T& operator*() {
        return ringBuffer.get(position);
    }

    private:
        int position;
        ring& ringBuffer;
};