#include <iostream>
#include <vector>

#include "ring.h"

using namespace std;

int main() {
    ring<string> textRing(3);

    textRing.add("one");
    textRing.add("two");
    textRing.add("three");
    textRing.add("four");
    textRing.add("five");

    for (int i = 0; i < textRing.getSize(); ++i) {
        cout << textRing.get(i) << endl;
    }
    cout << endl;

    // Need iterator
    for (ring<string>::iterator it=textRing.begin(); it != textRing.end(); ++it) {
        cout << *it << endl;
    }
    cout << endl;

    for (auto val : textRing) {
        cout << val << endl;
    }

    // ring<string>::iterator it;
    // it.print();
    return 0;
}