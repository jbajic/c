#include <iostream>
using namespace std;

class Parent {
    public:
    void speak() {
        cout << "Parent" << endl;
    }
};

class Brother: public Parent {

};

class Sister: public Parent {

};

int main() {
    Parent parent;
    Brother brother;

    float value = 3.23;

    //C style cast
    cout << (int) value << endl;
    //C++ style cast
    cout << static_cast<int>(value) << endl;

    Parent *pp = &brother;

    Brother *pb = static_cast<Brother*>(&parent);

    cout << pb << endl;

    Parent *ppb = &brother;
    Brother *pbb = static_cast<Brother*>(ppb);

    Parent &&p = Parent();
    Parent &&ppp = static_cast<Parent&&>(parent);
    ppp.speak();

    return 0;
}