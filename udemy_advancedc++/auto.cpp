#include <iostream>
using namespace std;

template<class T>
T test(T value) {
    return value;
}

// Trailing type
template<class T, class S>
auto test(T value1, S value2) -> decltype(value1 + value2) {
    return value1 + value2;
}

// template<class T>
// auto test(T value) -> decltype(value) {
//     return value;
// }

int main() {
    auto a = "ladida";

    cout << test(3,4) << endl;
    return 0;
}