#include <iostream>


#include "FractalCreator.hpp"
#include "RGB.hpp"

using namespace std;
using namespace bitmapSpace;

int main() {
    FractalCreator fractalCreator(800, 600);

    fractalCreator.addRange(0, RGB(0, 0, 0));
    fractalCreator.addRange(0.3, RGB(0, 128, 0));
    fractalCreator.addRange(0.5, RGB(64, 255, 64));
    fractalCreator.addRange(1.0, RGB(128, 255, 255));

//    cout << fractalCreator.getRange(310) << endl;

    fractalCreator.addZoom(Zoom(295, 202, 0.1));
    fractalCreator.addZoom(Zoom(312, 304, 0.1));

    fractalCreator.run("/home/bajic/test.bmp");

    cout << "Finished!" << endl;
    return 0;
}