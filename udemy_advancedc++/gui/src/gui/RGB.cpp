#include "RGB.hpp"

namespace bitmapSpace {

    RGB::RGB(double r, double g, double b): red(r), green(g), blue(b) {};

    RGB operator-(const RGB &first, const RGB &second) {
        return RGB(first.red - second.red, first.green - second.green, first.blue - second.blue);
    }


}
