#include "FractalCreator.hpp"
#include <iostream>

namespace bitmapSpace {

    FractalCreator::FractalCreator(int width, int height) : m_width(width),
                                                            m_height(height),
                                                            m_histogram(new int[Mandelbrot::MAX_ITERATIONS]{0}),
                                                            m_fractals(new int[m_height * m_width]{0}),
                                                            m_bitmap(m_width, m_height),
                                                            m_zoomList(m_width, m_height),
                                                            m_total{0} {
        m_zoomList.add(Zoom(m_width / 2, m_height / 2, 4.0 / m_width));
    }

    void FractalCreator::run(string name) {
        calculateIterations();
        calculateTotalIterations();
        calculateRangeTotals();
        drawFractal();
        writeBitmap(name);
    }

    void FractalCreator::addRange(double rangeEnd, const RGB &rgb) {
        m_ranges.push_back(rangeEnd * Mandelbrot::MAX_ITERATIONS);
        m_colors.push_back(rgb);

        if (m_bGotFirstRange) {
            m_rangeTotals.push_back(0);
        }
        m_bGotFirstRange = true;
    }

    int FractalCreator::getRange(int iterations) const {
        int range{0};

        for (int i = 1; i < m_ranges.size(); i++) {
            range = i;
            if (m_ranges[i] > iterations) {
                break;
            }
        }

        range--;

        assert(range > -1);
        assert(range < m_ranges.size());

        return range;
    }

    void FractalCreator::addZoom(const Zoom &zoom) {
        m_zoomList.add(zoom);
    }

    void FractalCreator::calculateIterations() {
        for (int y = 0; y < m_height; y++) {
            for (int x = 0; x < m_width; x++) {
                pair<double, double> coords = m_zoomList.doZoom(x, y);

                int iterations = Mandelbrot::getIterations(coords.first, coords.second);

                m_fractals[y * m_width + x] = iterations;

                if (iterations != Mandelbrot::MAX_ITERATIONS) {
                    m_histogram[iterations]++;
                }
            }
        }
    }

    void FractalCreator::drawFractal() {
        RGB startColor(0, 0, 0);
        RGB endColor(0, 0, 255);
        RGB colorDiff = endColor - startColor;

        for (int y = 0; y < m_height; y++) {
            for (int x = 0; x < m_width; x++) {

                int iterations = m_fractals[y * m_width + x];

                int range = getRange(iterations);
                int rangeTotal = m_rangeTotals[range];
                int rangeStart = m_ranges[range];

                RGB &startColor = m_colors[range];
                RGB &endColor = m_colors[range + 1];
                RGB colorDiff = endColor - startColor;

                uint8_t red{0};
                uint8_t green{0};
                uint8_t blue{0};

                // TODO use modern cast
//                uint8_t color = (uint8_t)(256 * (double) iterations / Mandelbrot::MAX_ITERATIONS);

                if (iterations != Mandelbrot::MAX_ITERATIONS) {
                    int totalPixels{0};

                    for (int i = rangeStart; i <= iterations; i++) {
                        // TODO smart cast
                        totalPixels += m_histogram[i];
                    }
                    red = startColor.red + colorDiff.red * (((double) totalPixels) / rangeTotal);
                    green = startColor.green + colorDiff.green * (((double) totalPixels) / rangeTotal);
                    blue = startColor.blue + colorDiff.blue * (((double) totalPixels) / rangeTotal);
                }

                m_bitmap.setPixel(x, y, red, green, blue);

            }
        }
    }

    void FractalCreator::calculateTotalIterations() {
        for (int i = 0; i < Mandelbrot::MAX_ITERATIONS; i++) {
            m_total += m_histogram[i];
        }

    }

    void FractalCreator::calculateRangeTotals() {
        int rangeIndex = 0;
        for (int i = 0; i < Mandelbrot::MAX_ITERATIONS; ++i) {
            int pixels = m_histogram[i];

            if (i >= m_ranges[rangeIndex + 1]) {
                rangeIndex++;
            }

            m_rangeTotals[rangeIndex] += pixels;
        }
    }

    void FractalCreator::writeBitmap(std::string name) {
        m_bitmap.write(name);
    }
}