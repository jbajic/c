#include <fstream>
#include "bitmap/Bitmap.hpp"
#include "bitmap/BitmapInfoHeader.hpp"
#include "bitmap/BitmapFileHeader.hpp"

using namespace bitmapSpace;
using namespace std;

namespace bitmapSpace {

    // If "new" fails catch it, some compilers throw exceptions (GNU)
    Bitmap::Bitmap(int width, int height) : m_width{width}, m_height{height},
                                            m_pPixels(new uint8_t[width * height * 3]{}) {
        //TODO
    }

    bool Bitmap::write(string filename) {
        BitmapInfoHeader bitmapInfoHeader;
        BitmapFileHeader bitmapFileHeader;

        bitmapFileHeader.fileSize = sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader) + m_width * m_height * 3;
        bitmapFileHeader.dataOffset = sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader);

        bitmapInfoHeader.width = m_width;
        bitmapInfoHeader.height = m_height;

        ofstream file;
        file.open(filename, ios::out|ios::binary);

        if (!file) {
            return false;
        }

        file.write((char *)&bitmapFileHeader, sizeof(bitmapFileHeader));
        file.write((char *)&bitmapInfoHeader, sizeof(bitmapInfoHeader));
        file.write((char *)m_pPixels.get(), m_width * m_height * 3);

        file.close();
        if (!file) {
            return false;
        }
        return true;
    }

    void Bitmap::setPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue) {
        // Wat to cast unique ptr to normal is by using get on unique ptr
        uint8_t *pPixel = m_pPixels.get();

        pPixel += y * 3 * m_width + x * 3;

        // Bitmap is little endian
        // 0xFF8833 => write order: 3388ff
        pPixel[0] = blue;
        pPixel[1] = green;
        pPixel[2] = red;
    }

    Bitmap::~Bitmap() {
        //TODO
    }

} // namespace bitmapSpace