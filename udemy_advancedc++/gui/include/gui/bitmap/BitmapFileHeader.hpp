#ifndef C_BITMAPFILEHEADER_H
#define C_BITMAPFILEHEADER_H

#include <cstdint>

using namespace std;
// Makes C++ align everything on 2 bytes
#pragma pack(2)

namespace bitmapSpace
{

struct BitmapFileHeader
{
    char header[2]{'B', 'M'};
    int32_t fileSize;
    int32_t reserved{0};
    int32_t dataOffset;
};

} // namespace bitmapSpace
#endif //C_BITMAPFILEHEADER_H
