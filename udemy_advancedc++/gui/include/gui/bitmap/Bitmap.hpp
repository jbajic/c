#ifndef C_BITMAP_H
#define C_BITMAP_H

#include <string>
#include <cstdint>
#include <memory>

using namespace std;

namespace bitmapSpace
{

class Bitmap
{
private:
    int m_width{0};
    int m_height{0};
    std::unique_ptr<uint8_t[]> m_pPixels{nullptr};

public:
    Bitmap(int width, int height);
    bool write(string filename);
    void setPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue);
    virtual ~Bitmap();
};

} // namespace bitmapSpace
#endif //C_BITMAP_H
