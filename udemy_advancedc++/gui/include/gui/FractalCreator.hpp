#ifndef C_FRACTALCREATOR_H
#define C_FRACTALCREATOR_H

#include <assert.h>
#include <math.h>

#include <string>
#include <cstdint>
#include <memory>
#include <vector>

#include "Zoom.hpp"
#include "bitmap/Bitmap.hpp"
#include "ZoomList.hpp"
#include "fractals/Mandelbrot.hpp"
#include "RGB.hpp"

namespace bitmapSpace {
    class FractalCreator {

    private:
        int m_width;
        int m_height;
        unique_ptr<int[]> m_histogram;
        unique_ptr<int[]> m_fractals;
        Bitmap m_bitmap;
        ZoomList m_zoomList;
        int m_total;

        vector<int> m_ranges;
        vector<RGB> m_colors;
        vector<int> m_rangeTotals;

        bool m_bGotFirstRange{false};
    private:
        void calculateIterations();

        void calculateTotalIterations();

        void calculateRangeTotals();

        void drawFractal();

        void writeBitmap(std::string name);

        int getRange(int iterations) const;

    public:

        FractalCreator(int width, int height);

        void addRange(double rangeEnd, const RGB &rgb);

        void run(string name);

        void addZoom(const Zoom &zoom);
    };
}

#endif //C_FRACTALCREATOR_H
