#ifndef C_RGB_H
#define C_RGB_H

namespace bitmapSpace {

    struct RGB {
        double red;
        double green;
        double blue;
        RGB(double r, double g, double b);
    };
    RGB operator-(const RGB &first, const RGB &second);
}

#endif //C_RGB_H
