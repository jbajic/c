#ifndef C_ZOOM_H
#define C_ZOOM_H

namespace bitmapSpace {

    struct Zoom {
        int x{0};
        int y{0};
        double scale{0.0};

        Zoom(int x, int y, double scale): x(x), y(y), scale(scale) {};
    };
}

#endif //C_ZOOM_H
