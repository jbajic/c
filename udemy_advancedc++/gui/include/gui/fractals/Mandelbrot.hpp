#ifndef C_MANDELBROT_H
#define C_MANDELBROT_H

namespace bitmapSpace {

    class Mandelbrot {
    public:
        static const int MAX_ITERATIONS = 1000;
    public:
        Mandelbrot();

        virtual ~Mandelbrot();

        static int getIterations(double x, double y);
    };
}


#endif //C_MANDELBROT_H
