#include <iostream>
#include <vector>

using namespace std;

int main() {
    auto texts = {"one", "two", "three"};

    for (auto text: texts) {
        cout << text << endl;
    }

    vector<int> numvs;
    numvs.push_back(1);
    numvs.push_back(2);
    numvs.push_back(3);
    numvs.push_back(4);
    numvs.push_back(5);
    for(auto num: numvs) {
        cout << num << endl;
    }
    string test = "ladida";
    for(auto c: test) {
        cout << c << endl;
    }
    return 0;
}