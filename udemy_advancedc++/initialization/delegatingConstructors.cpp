#include <iostream>
using namespace std;

class Parent {
    int dogs{5};
    string text{"hello"};

    public:
        Parent(): Parent("Hello") {
            cout << "No parameter parent constructor" << endl;
        }

        Parent(string text) {
            this->text = text;
            cout << "string parent constructor" << endl;
        }
};

class Child: public Parent {
    public:
    Child(): Parent("Hello") {}
};

int main() {
    Parent parent();
    Child child;
    return 0;
}