#include <iostream>
#include <vector>
#include <initializer_list>

using namespace std;

class dummyClass {
    public:
    dummyClass(initializer_list<string> texts) {
        for(auto text:texts) {
            cout << text << endl;
        }
    }

    void print(initializer_list<string> list) {
        for (auto value: list) {
            cout << value << endl;
        }
    }
};

class Test{
    int id{1};
    string name{"Noname"};
    public:
    Test() = default;
    Test(int _id): id(_id) {}

    // Test(const Test&) = default;
    Test(const Test&) = delete;

    Test& operator=(const Test& other) = delete;

    void print() {
        cout << id << ": " << name << endl;
    }
};

int main() {
    vector<int> vectorExample {1, 2, 3};
    cout << vectorExample[2];

    dummyClass ladida {"yes", "no", "maybe", "idont", "know"};
    ladida.print({"bla", "asasas"});

    Test test;
    test.print();

    Test test1(12);
    test1.print();
    return 0;
}