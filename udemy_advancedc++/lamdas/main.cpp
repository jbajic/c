#include <iostream>

using namespace std;

void test(void (*pFunc)()) {
    pFunc();
}

int main() {
    [](){cout << "Hello lamda!" << endl;}();
    auto func = [](){cout << "Hello lamda!" << endl;};

    func();
    test(func);
    return 0;
}