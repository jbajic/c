#include <iostream>

using namespace std;

int main() {
    int one = 1;
    int two = 2;
    int three = 3;

    // Capture all local variables by value
    [=](){
        cout << one << two << three << endl;
    }();
    
    // Capture local variables by value
    [one, two, three](){
        cout << one << two << three << endl;
    }();

    // Capture all local vars by value but three by reference
    [=, &three](){
        three = 5;
        cout << one << two << three << endl;
    }();

    // Capture all local vars by reference
    [&](){
        one = 3;
        two = 2;
        three = 1;
        cout << one << two << three << endl;
    }();

    [=](){
        cout << one << two << three << endl;
    }();

    return 0;
}