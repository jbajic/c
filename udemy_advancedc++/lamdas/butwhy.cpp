#include <iostream>

using namespace std;

void test(void (*pFunc)()) {
    pFunc();
}

int main() {
    string cat = "cat";

    auto ladida = [cat]() mutable {
        cat = "dog";
        cout << cat << endl;
    };
    ladida();
    return 0;
}