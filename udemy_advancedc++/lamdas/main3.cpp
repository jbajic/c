#include <iostream>

using namespace std;

class Test {
private:
    int one{1};
    int two{2};

public:
    void run() {
        int three{3};
        int four{4};
        // This is captured by reference since this is a pointer
        auto pLambda = [this, three, four](){
            this->one = 45;
            cout << three << four << endl;
            cout << this->one << this->two << endl;
        };

        pLambda();
    }
};

int main() {
    Test test;
    test.run();
    return 0;
}