#include <iostream>

using namespace std;

void testGreet(void (*funcPointer)(string)) {
    funcPointer("Netko");
}

void runDivide(double (*pDivide)(double, double)) {
    cout << pDivide(4,2) << endl;
}

int main() {
    auto pGreet = [](string name){cout << "Hello " << name << endl;};

    pGreet("Jure");
    testGreet(pGreet);
    // Trailing return type
    auto pDivide = [](double a, double b) -> double{
        if (b == 0) {
            return 0;
        } else {
            return a / b;
        }
    };

    cout << pDivide(3.14, 2.18) << endl;
    runDivide(pDivide);
    return 0;
}