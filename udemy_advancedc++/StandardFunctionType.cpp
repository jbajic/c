#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>

using namespace std;

class Check{
    public:
    bool operator()(string& test) {
        return test.size() == 2;
    }
} check1;

bool check(string &test) {
    return test.size() == 3;
}

void run(function<bool(string&)> check) {
    string test = "dog";
    cout << check(test) << endl;
}

int main() {
    vector<string> vec{"one", "two", "three"};
    int size = 5;
    auto lamdaFun = [size](string test){return test.size() == size;};

    cout 
        << count_if(vec.begin(), vec.end(), lamdaFun)
        << endl;

    cout 
        << count_if(vec.begin(), vec.end(), check)
        << endl;

    cout 
        << count_if(vec.begin(), vec.end(), check1) 
        << endl;

    run(lamdaFun);
    run(check);
    run(check1);

    function<int(int, int)> add = [] (int one, int two) {
        return one + two;
    };
    cout << add(7, 3) << endl;
    return 0;
}