#include <iostream>
#include <vector>
#include <memory.h>
using namespace std;

class Test {
    private:
        static const int SIZE = 100;

        int *_pBuffer{nullptr};
    public:
        Test() {
            _pBuffer = new int[SIZE]{};
        }

        Test(int i) {
            _pBuffer = new int[SIZE]{};

            for (int i = 0; i < SIZE; ++i) {
                _pBuffer[i] = 7 * i;
            }
        }

        Test(const Test& other) {
            _pBuffer = new int[SIZE]{};
            memcpy(_pBuffer, other._pBuffer, sizeof(int) * SIZE);
        }

        //Move contructor
        Test(Test&& other) {
            cout << "Move constructor" << endl;
            _pBuffer = other._pBuffer;
            other._pBuffer = nullptr;
        }

        //Move assigment
        Test& operator=(Test &&other) {
            cout << "Move assigment" << endl;
            // Delete curently allocated memory of lhs
            delete[] _pBuffer;
            // assing memory of lhs to point to rhs
            _pBuffer = other._pBuffer;
            //stop rhs from deleting the memory, by pointing tit to nullptr
            other._pBuffer = nullptr;
            return *this;
        }

        Test operator=(const Test& other) {
            _pBuffer = new int[SIZE]{};
            memcpy(_pBuffer, other._pBuffer, sizeof(int) * SIZE);
            return *this;
        }

        ~Test() {
            delete[] _pBuffer;
        }

        friend ostream &operator<<(ostream &out, const Test &test);
};

ostream &operator<<(ostream& out, const Test& test) {
    return out;
}

Test getTest() {
    return Test();
}

int main() {
    vector<Test> vec;
    vec.push_back(Test());

    Test test;
    test = getTest();

    return 0;
}
