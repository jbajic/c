#include <algorithm>
#include <iostream>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

auto backtrack(int start, int end, unordered_map<int, int> parent_map) {
  int currentNode = end;
  vector<int> path{currentNode};
  while (currentNode != start) {
    currentNode = parent_map[currentNode];
    path.push_back(currentNode);
  }
  return path;
}

auto bfs(int start, int end, unordered_map<int, vector<int>> &graph) {
  // int shortestPath{0};
  unordered_map<int, int> parent_map;
  vector<int> nodes;
  unordered_set<int> visited;
  nodes.push_back(start);

  while (!nodes.empty()) {
    int currentNode = nodes.back();
    visited.insert(currentNode);
    nodes.pop_back();
    if (currentNode == end) {
      // return shortestPath;
      return backtrack(start, end, parent_map);
    }
    for (auto const neighbour : graph[currentNode]) {
      if (visited.find(neighbour) == visited.end()) {
        nodes.push_back(neighbour);
        parent_map[neighbour] = currentNode;
      }
    }
    // shortestPath++;
  }

  return vector<int>{};
}

auto dfs(int start, int end, unordered_map<int, vector<int>> &graph) {
  unordered_map<int, int> parent_map;
  stack<int> nodes;
  unordered_set<int> visited;
  nodes.push(start);

  while (!nodes.empty()) {
    int currentNode = nodes.top();
    nodes.pop();
    visited.insert(currentNode);
    auto neighbors = graph[currentNode];
    for (auto neighbour : neighbors) {
      if (visited.find(neighbour) == visited.end()) {
        nodes.push(neighbour);
        parent_map[neighbour] = currentNode;
        if(neighbour == end) return backtrack(start, end, parent_map);
      }
    }
  }
  return vector<int>{};
}

int main() {
  unordered_map<int, vector<int>> graph{
      {0, {1}}, {1, {2, 3}}, {2, {3}},      {3, {6, 7, 8}}, {4, {5}},
      {5, {0}}, {7, {9}},    {8, {10, 12}}, {10, {11}},
  };
  for (const auto &[index, neighbors] : graph) {
    cout << "Vertex " << index << ": ";
    for (const auto elem : neighbors) {
      cout << elem << ", ";
    }
    cout << "\n";
  }
  auto shortestPath = dfs(0, 11, graph);
  // cout << "Shortest Path: " << shortestPath << endl;
  for (auto elem : shortestPath) {
    cout << elem << " -> ";
  }
  cout << endl;
  return 0;
}