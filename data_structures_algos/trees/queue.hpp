#pragma once
#include <iostream>

using namespace std;

struct Node {
    int data;
    Node* lchild;
    Node* rchild;
    Node(int x): data{x}, lchild{nullptr}, rchild{nullptr} {}
};

class Queue {
    private:
    int front;
    int rear;
    int size;
    Node **Q;

    public:

    Queue();

    Queue(int size);

    void enqueue(Node* data);

    bool isEmpty();

    Node* dequeue();

    void display();
};
