#include "queue.hpp"

Queue::Queue(): front{-1}, rear{-1}, size{8}, Q{new Node*[size]} {}

Queue::Queue(int size): front{-1}, rear{-1}, size{size}, Q{new Node*[size]} {}

void Queue::enqueue(Node* data) {
    if(rear == size-1) {
        cout << "Queue Full!" << endl;
    } else {
        rear++;
        Q[rear] = data;
    }
}

Node* Queue::dequeue() {
    Node* x{nullptr};
    if(front == rear) {
        cout << "Queue is empty!" << endl;
    } else {
        x = Q[++front];
    }
    return x;
}

bool Queue::isEmpty() {
    return front == rear;
}

void Queue::display() {
    for(int i = front + 1 ; i <= rear; ++i) {
        cout << Q[i] << ", ";
    }
    cout << endl;
}
