#include <iostream>
#include <stack>
#include "queue.hpp"

using namespace std;

class Tree{
    public:
    Node* root;

    Tree():root{nullptr} {}

    void CreateTree() {
        Node*p, *t;
        int x;
        Queue q(100);

        cout << "Enter root" << endl;
        cin >> x;
        root = new Node(x);
        q.enqueue(root);
        while(!q.isEmpty()) {
            p = q.dequeue();
            cout << "Enter left child node!" << endl;
            cin >> x;
            if(x != -1) {
                t = new Node(x);
                p->lchild = t;
                q.enqueue(t);
            }
            cout << "Enter right child node!" << endl;
            cin >> x;
            if(x != -1) {
                t = new Node(x);
                p->rchild = t;
                q.enqueue(t);
            }
        }
    }

    void Preorder(Node *p) {
        if(p) {
            cout << p->data << ", ";
            Preorder(p->lchild);
            Preorder(p->rchild);
        }
    }

    void PreorderIter(Node *p) {
        stack<Node*> nodes;
        while(p != nullptr || !nodes.empty()) {
            if(p != nullptr) {
                cout << p->data << ", ";
                nodes.push(p);
                p = p->lchild;
            } else {
                p = nodes.top();
                nodes.pop();
                p = p->rchild;
            }
        }
    }

    void Inorder(Node *p) {
        if(p) {
            Inorder(p->lchild);
            cout << p->data << ", ";
            Inorder(p->rchild);
        }
    }

    void Postorder(Node *p) {
        if(p) {
            Postorder(p->lchild);
            Postorder(p->rchild);
            cout << p->data << ", ";
        }
    }

    void Levelorder(Node *root) {
        Queue q(100);
        cout << root->data << ", ";
        q.enqueue(root);
        while(!q.isEmpty()) {
            root = q.dequeue();
            if(root->lchild) {
                cout << root->lchild->data << ", ";
                q.enqueue(root->lchild);
            }
            if(root->rchild) {
                cout << root->rchild->data << ", ";
                q.enqueue(root->rchild);
            }
        }
    }

    int Height(Node *root) {
        int x{0}, y{0};
        if(root == nullptr) {
            return 0;
        }
        x = Height(root->lchild);
        y = Height(root->rchild);
        if (x > y)
            return x+1;
        else
            return y+1;
    }

    int count(Node *root) {
        if(root != nullptr) {
            return count(root->lchild) + count(root->rchild) + 1;
        }
        return 0;
    }

    int countDeg2Or1(Node *root) {
        int x, y;
        if(root->lchild != nullptr || root->rchild != nullptr) {
            x = countLeafs(root->lchild);
            y = countLeafs(root->rchild);
            return x + y + 1;
        }
        return 0;
    }

    int countDeg2(Node *root) {
        int x{0}, y{0};
        if(root != nullptr) {
            x = countLeafs(root->lchild);
            y = countLeafs(root->rchild);
            if(root->lchild != nullptr && root->rchild != nullptr)
                return x + y + 1;
            else
                return x + y;
        }
        return 0;
    }

    int countLeafs(Node *root) {
        int x, y;
        if(root != nullptr) {
            x = countLeafs(root->lchild);
            y = countLeafs(root->rchild);
            if(root->lchild == nullptr && root->rchild == nullptr)
                return x + y + 1;
            else
                return x + y;
        }
        return 0;
    }

};

int main() {
    Tree tree;
    tree.CreateTree();
    cout << "Preorder: ";
    tree.Preorder(tree.root);
    cout << endl;
    cout << "Inorder: ";
    tree.Inorder(tree.root);
    cout << endl;
    cout << "Postorder: ";
    tree.Postorder(tree.root);
    cout << endl;
    cout << "Levelorder: ";
    tree.Levelorder(tree.root);
    cout << endl;
    cout << "Pred order iter: ";
    tree.PreorderIter(tree.root);
    cout << endl;
    cout << "Number of nodes: " << tree.count(tree.root) << endl;
    cout << "Number of leafs: " << tree.countLeafs(tree.root) << endl;
    cout << "Number of nodes degree 2: " << tree.countDeg2(tree.root) << endl;
    cout << "Number of nodes degree 1 or 2: " << tree.countDeg2Or1(tree.root) << endl;
    return 0;
}
