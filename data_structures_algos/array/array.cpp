#include <iostream>
#include <initializer_list>
using namespace std;

template <typename Element>
class Array
{
public:
    Array() : arr{nullptr}, length{0} {}
    Array(const std::initializer_list<Element> &list, Element length) : length{length}
    {
        if (length >= size)
        {
            expandArray();
        }
        else
        {
            arr = new Element[size];
        }
        int counter = 0;
        for (auto &element : list)
        {
            arr[counter] = element;
            counter++;
        }
    }
    Array(int length) : length{length}
    {
        if (length >= size)
        {
            expandArray();
        }
        else
        {
            arr = new Element[size];
        }
    }
    ~Array()
    {
        if (arr != nullptr)
            delete[] arr;
    }

    void insert(Element num)
    {
        if (length >= size)
        {
            if (length >= size)
                expandArray();
        }
        arr[length] = num;
        length++;
    }

    void insert(int index, Element num)
    {
        if (index < length)
        {

            if (length + 1 >= size)
                expandArray();
            for (int i = length; i > index; --i)
            {
                arr[i] = arr[i - 1];
            }
            arr[index] = num;
            length++;
        }
    }

    void deleteAt(int index)
    {
        if (index < length)
        {
            for (int i = index; i < length - 1; ++i)
            {
                arr[i] = arr[i + 1];
            }
            length--;
        }
    }

    int search(Element elem) const
    {
        for (int i = 0; i < length; ++i)
        {
            if (arr[i] == elem)
                return i;
        }
        return -1;
    }

    void display() const
    {
        for (int i = 0; i < length; ++i)
        {
            cout << arr[i] << ", ";
        }
        cout << endl;
    }

    void sort()
    {
        quickSort(0, length);
    }

    void quickSort(int low, int high)
    {
        if (high > low)
        {
            int pivotIndex = partition(low, high);

            quickSort(low, pivotIndex - 1);
            quickSort(pivotIndex + 1, high);
        }
    }

    int partition(int low, int high)
    {
        int pivotElement = arr[high];
        int pivotIndex = low;
        for (int i = low; i < high; ++i)
        {
            if (arr[i] <= pivotElement)
            {
                std::swap(arr[i], arr[pivotIndex]);
                pivotIndex++;
            }
        }
        std::swap(arr[pivotIndex], arr[high]);
        return pivotIndex;
    }

    int binarySearch(Element elem) const
    {
        int indexLow{0};
        int indexHigh{length};
        int indexMiddle;
        while (indexLow <= indexHigh)
        {
            indexMiddle = (indexHigh + indexLow) / 2;
            if (arr[indexMiddle] == elem)
            {
                return indexMiddle;
            }
            if (arr[indexMiddle] > elem)
            {
                indexHigh = indexMiddle - 1;
            }
            else
            {
                indexLow = indexMiddle + 1;
            }
        }
        return -1;
    }

    void reverse()
    {
        //1. Use additional array 2. this
        int i, j;
        for (i = 0, j = length - 1; i != j; i++, j--)
        {
            std::swap(arr[i], arr[j]);
        }
    }

private:
    Element *arr;
    int length;
    int size = 32;

    void expandArray()
    {
        while (size < length)
        {
            size *= 2;
        }
        if (arr != nullptr)
            delete[] arr;
        arr = new Element[size];
    }
};

int main()
{
    Array<int> array{{1, 2, 3, 4, 5}, 5};
    array.display();
    cout << "Insert nubmer from 6-10" << endl;
    for (int i = 6; i < 10; ++i)
    {
        array.insert(i);
    }
    array.display();
    cout << "Insert 4 at 0 index" << endl;
    array.insert(0, 4);
    array.display();
    cout << "Delete element at 5 index" << endl;
    array.deleteAt(5);
    array.display();
    cout << "Index of nubmer 7 is: " << array.search(7) << endl;
    cout << "Sort Array" << endl;
    array.sort();
    array.display();
    cout << "Index of nubmer 3 is " << array.binarySearch(9) << endl;
    cout << "Reverse array" << endl;
    array.reverse();
    array.display();
    return 0;
}