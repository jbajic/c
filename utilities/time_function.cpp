// Given a linked list, check if the linked list has loop or not. Below diagram
// shows a linked list with a loop.
#include <iostream>
#include <memory>
#include <unordered_set>
#include <chrono>

template<typename TimeT = std::chrono::microseconds>
struct measure
{
    template<typename F, typename ...Args>
    static typename TimeT::rep execution(F&& func, Args&&... args)
    {
        auto start = std::chrono::steady_clock::now();
        std::forward<decltype(func)>(func)(std::forward<Args>(args)...);
        auto duration = std::chrono::duration_cast< TimeT> 
                            (std::chrono::steady_clock::now() - start);
        return duration.count();
    }
};
using namespace std;

int fibonacci_series(int n)
{
  if (n < 2)
  {
    return n;
  }
  else
  {
    return fibonacci_series(n - 1) + fibonacci_series(n - 2);
  }
}

int main()
{
  cout << measure<std::chrono::microseconds>::execution(fibonacci_series, 45) << endl;
  return 0;
}