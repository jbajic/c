#include <iostream>

struct Node
{
    class NodeIterator;
    Node() {}

    Node(int e, Node *n) : elem(e), next(n) {}

    int elem = 0;
    Node *next = nullptr;

    class NodeIterator
    {
    private:
        Node *current{nullptr};

    public:
        NodeIterator() {}
        NodeIterator(Node *node) : current{node} {}

        NodeIterator &operator++()
        {
            if (current)
                current = current->next;
            return *this;
        }

        bool operator==(const NodeIterator &it)
        {
            return current == it.current;
        }

        bool operator!=(const NodeIterator &it)
        {
            return current != it.current;
        }

        int& operator*() const
        {
            return current->elem;
        }

    };

    NodeIterator begin() const
    {
        return NodeIterator(next);
    }

    NodeIterator end() const
    {
        return NodeIterator();
    }

};

void add(Node &n, int x)
{
    n.next = new Node(x, n.next);
}

int main()
{
    Node head;
    add(head, 1);
    add(head, 2);
    add(head, 3);
    add(head, 4);
    add(head, 5);

    for (Node *p = head.next; p != nullptr; p = p->next)
    {
        std::cout << p->elem << " ";
    }
    std::cout << std::endl;

    for (int x : head)
    {
        std::cout << x << " ";
    }
    std::cout << std::endl;
    for (int &x : head)
    {
        x = 7;
    }
    for (const int &x : head)
    {
        std::cout << x << " ";
    }
    std::cout << std::endl;

    return 0;
}
