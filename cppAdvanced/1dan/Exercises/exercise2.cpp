﻿#include <iostream>

// Here implement the following function:
// int fibo(int n)

constexpr int fibo(int n) {
   if( (n == 1) || (n == 0)) {
      return n;
   }else {
      return (fibo(n - 1) + fibo(n - 2));
   }
}

int main()
{
	static_assert(fibo(7) == 34);
	const int k = fibo(9);
	std::cout << k << std::endl;
	// const int l = fibo(300); // 300. Fibonacci number is too big for int
}
