#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>

using namespace std;

mutex var1, var2;

void writer(int &a, int &b) {
    int i{0};
    pair<int, int> pairs[] = {
            {1, 2},
            {3, 4},
            {5, 6}
    };
    while (i < 3) {
        scoped_lock(var1, var2);
        a = pairs[i].first;
        b = pairs[i].second;
        i++;
        if (i == 3) i = 0;
    }
}

void sumNum(int &a, int &b) {
    while (true) {
        scoped_lock(var1, var2);
        cout << a + b << endl;
    }
}

int main() {
    int a{0}, b{0};
    thread write_t(writer, std::ref(a), std::ref(b));
    thread sum_t(sumNum, std::ref(a), std::ref(b));

    write_t.join();
    return 0;
}