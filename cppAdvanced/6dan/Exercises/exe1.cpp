#include <iostream>
#include <thread>
#include <chrono>

using namespace std;

int print(int num) {
    this_thread::sleep_for(2s);
    cout << num << endl;
    return 0;
}

int print_and_wait(int num, thread&& th) {
    th.join();
    cout << num << endl;
    return 0;
}

int main() {
    thread thread1(print, 1);
    thread thread2(print_and_wait, 2, move(thread1));
    thread thread13(print_and_wait, 3, move(thread2));
    thread13.join();
    return 0;
}