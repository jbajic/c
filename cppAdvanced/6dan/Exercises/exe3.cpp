#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>

using namespace std;

struct Semaphore {
    Semaphore() = default;

    Semaphore(int x): m_s(x) {}

    void signal() {
        std::unique_lock<std::mutex> lock(m_mut);
        m_s += 1;
        m_cv.notify_one();
    }

    void wait() {
        std::unique_lock<std::mutex> lock(m_mut);
        m_cv.wait(lock, [this]() { return m_s != 0; });
        m_s -= 1;
    }

private:
    int m_s = 0;
    std::mutex m_mut;
    std::condition_variable m_cv;
};

struct RingBuffer {
    void write(char x) {
        m_empty.wait();
        std::lock_guard<mutex> l(m_mut);

        m_buff[m_w] = x;

        m_w = (m_w % 10 == 0)? 0: m_w + 1;
        m_full.signal();
    }

    int read() {
        m_full.wait();
        std::lock_guard<mutex> l(m_mut);
        char temp;

        temp = m_buff[m_r];

        m_r = (m_r % 10 == 0)? 0: m_r + 1;
        m_empty.signal();
        return temp;
    }

private:
    std::array<char, 10> m_buff;
    int m_w = 0;
    int m_r = 0;

    Semaphore m_empty{10};
    Semaphore m_full;
    std::mutex m_mut;
};

void readIn(RingBuffer& input) {
    while(true) {
        char character;
        cin >> character;
        input.write(character);
        if (character == '#') {
            // cout << "Break read in" << endl;
            break;
        }
    }
}

void transform(RingBuffer& input, RingBuffer& output) {
    while(true) {
        char character = input.read();
        if (character > 96 && character < 123) {
            character -= 32;
        }
        output.write(character);
        if(character == '#') {
            // cout << "Break transform" << endl;
            break;
        }
    }
}

void printOut(RingBuffer& output) {
    while(true) {
        char character = output.read();
        cout << character << endl;
        if(character == '#') {
            // cout << "Break print out" << endl;
            break;
        }
    }
}

int main() {
    RingBuffer input;
    RingBuffer output;

    thread readInput_t(readIn, std::ref(input));
    thread writeInput_t(printOut, std::ref(output));
    thread transform_t(transform, std::ref(input), std::ref(output));

    readInput_t.join();
    transform_t.join();
    writeInput_t.join();

    return 0;
}