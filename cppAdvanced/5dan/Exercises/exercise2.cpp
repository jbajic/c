#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

using std::cout;
using std::vector;


template<typename T>
void printElements(const T &c) {
    for (const auto &x : c) {
        cout << x << " ";
    }
    cout << std::endl;
}

//template<typename T>
struct Functor {
        vector<int>& vec;
public:
    Functor(vector<int>& vec): vec{vec} {};

    int operator()() {
        return vec.size();
    }
};

//template<typename T>
struct Square_all {
public:
    Square_all() = default;

    void operator()(int& other) {
        other *= other;
    }
};

struct Square_sum {
    Square_sum() = default;
    int operator()(int& lfs, int& rhs) {
        return lfs + rhs*rhs;
    }
};

struct Show_evem {
    Show_evem() = default;
    void operator()(int& other) {
        if(other % 2 == 0) {
            cout << other << " ";
        }
    }
};

int main() {
    vector<int> first;
    generate_n(back_inserter(first), 100, Functor(first));
    cout << "First:\n";
    printElements(first);

    vector<int> second{first};  // Copy vector first to vector second

    // 2. Modify values of vector "first" by squaring them.
    std::for_each(first.begin(), first.end(), Square_all());

    cout << "\n\nSquares:\n";
    printElements(first);

    int res{
            std::accumulate(second.begin(), second.end(), 0, Square_sum())

            // 3. Calculate the sum of sqared elemenents of "second".

    };
    cout << "\n\nSum of squares: " << res << "\n";
    if (res != 328350)
        return 1; // wrong result

    cout << "\nEven numbers:\n";
    // 4. Print out only even numbers in vector "second".
    std::for_each(second.begin(), second.end(), Show_evem());
    return 0;
}
