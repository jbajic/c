#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

using std::cout;
using std::vector;


template<typename T>
void printElements(const T &c) {
    for (const auto &x : c) {
        cout << x << " ";
    }
    cout << std::endl;
}


int main() {
    vector<int> first;
    generate_n(back_inserter(first), 100, [&first]() {
        return first.size();
    });

    cout << "First:\n";
    printElements(first);

    vector<int> second{first};  // Copy vector first to vector second

    // 2. Modify values of vector "first" by squaring them.
    std::for_each(first.begin(), first.end(), [](int& i) {i *= i;});

    cout << "\n\nSquares:\n";
    printElements(first);

    int res{
        std::accumulate(second.begin(), second.end(), 0, [](int left, int right){return left + right*right;})

            // 3. Calculate the sum of sqared elemenents of "second".

    };
    cout << "\n\nSum of squares: " << res << "\n";
    if (res != 328350)
        return 1; // wrong result

    cout << "\nEven numbers:\n";
    // 4. Print out only even numbers in vector "second".
    std::for_each(second.begin(), second.end(), [](const int i){ if(i % 2 == 0) std::cout << i << " "; });
    return 0;
}
