#include <iostream>
#include <memory>
#include <string>

using namespace std;

struct Node {
	string name;
	shared_ptr<Node> left = nullptr;
	shared_ptr<Node> right = nullptr;
    shared_ptr<Node> parent;
    //Note: "parent Node"; ToDo: Here make the pointer

	Node(string x) : name(x){
		cout << "Constructor " << name << endl;
		parent.reset();
	}
	~Node() { cout << "Destructor " << name << endl; }

	string toString() {
		string lStr{ "<none>" }, rStr{ "<none>" }, pStr{ "<none>" };
		
		if (left != nullptr) lStr = left->toString();
		if (right != nullptr) rStr = right->toString();
		// if (parent != nullptr) rStr = parent->toString();
		if(auto temp = parent.lock()) pStr = temp->name;

        string res;
        res += "{Me:" + name + " ";
		res += "Parent:" + pStr + " "; //Note: Serves as a guide towards solution :)
		res += "Left:" + lStr + " ";
		res += "Right:" + rStr + "}";
		cout << "Laida:" << endl;
		return res;
	}
};


shared_ptr<Node> foo() {
	shared_ptr<Node> root = make_shared<Node>("root");
	root->left = make_shared<Node>("left");
    root->right = make_shared<Node>("right");
	root->left->parent = root;
	root->right->parent = root;
    //Note: root should be parent of root->left

    return root;
}


int main()
{
	shared_ptr<Node> k = foo();
	cout << k->toString() << endl;
	return 0;
}
