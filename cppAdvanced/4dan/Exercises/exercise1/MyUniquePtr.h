#ifndef _MY_UNIQUE_PTR_
#define _MY_UNIQUE_PTR_



template<typename T>
class MyUniquePtr {
private:
    T *ptr{nullptr};
public:
    MyUniquePtr(): ptr{nullptr} {};
// Whoops!
//    template<typename... Us>
//    MyUniquePtr(Us... vars) : ptr{new T{std::forward<Us>(vars)...}} {}
    MyUniquePtr(T* _ptr): ptr{std::move(_ptr)} {
        _ptr = nullptr;
    }

    ~MyUniquePtr() {
        std::cout << "Hi!" << std::endl;
        delete[] ptr;
    }

    MyUniquePtr(const MyUniquePtr &rhs) = delete;

    MyUniquePtr operator=(const MyUniquePtr &rhs) = delete;

    MyUniquePtr(MyUniquePtr &&rhs) : ptr{std::move(rhs.ptr)} {}

    MyUniquePtr &operator=(MyUniquePtr &&rhs) {
        ptr = rhs.ptr;
        rhs.ptr = nullptr;
        return *this;
    }

    T& operator*() {
        return *ptr;
    }

    T* operator-> ()
    {
        return ptr;
    }

    T* get() {
        return ptr;
    }
};

#endif
