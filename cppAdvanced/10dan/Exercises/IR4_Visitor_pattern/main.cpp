﻿/*
Interpretation of tree-like IR.
Program goes through the IR and searches for maximal number of arguments of print statement.

Version 4: Using inheritance and visitor pattern: functions that perform the processing are separated
from classes that describe the IR structure. Especially useful when there is a need to implement
several different tree walks. That is why additional visitor is added: that count all the nodes.
getKind is still not used.
*/

#include "IR.h"
#include "ArgsVisitor.h"
#include "NodesVisitor.h"

#include <iostream>

using std::cout;
using std::endl;


IrStm* prog1() {
	// a := 5+3; b := (print(a, a-1), 10*a); print(b);
	return
		makeCompoundStm(
			makeAssignStm("a", makeOpExp(makeNumExp(5), OK_PLUS, makeNumExp(3))),
			makeCompoundStm(
				makeAssignStm("b", makeEseqExp(
					makePrintStm(makePairExpList(makeIdExp("a"), makeLastExpList(
						makeOpExp(makeIdExp("a"), OK_MINUS, makeNumExp(1))))),
					makeOpExp(makeNumExp(10), OK_MUL, makeIdExp("a")))),
				makePrintStm(makeLastExpList(makeIdExp("b")))
			)
		);
}


int main()
{
	IrStm* root = prog1();
	cout << root->accept(ArgsVisitor()) << endl;
	cout << root->accept(NodesVisitor());

	return 0;
}
