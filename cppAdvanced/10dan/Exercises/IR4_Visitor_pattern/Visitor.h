#ifndef _VISITOR_H_
#define _VISITOR_H_


struct CompoundStm;
struct AssignStm;
struct PrintStm;

struct IdExp;
struct NumExp;
struct OpExp;
struct EseqExp;

struct PairExpList;
struct LastExpList;

struct Visitor {
	virtual int visit(CompoundStm& x) = 0;
	virtual int visit(AssignStm& x) = 0;
	virtual int visit(PrintStm& x) = 0;

	virtual int visit(IdExp& x) = 0;
	virtual int visit(NumExp& x) = 0;
	virtual int visit(OpExp& x) = 0;
	virtual int visit(EseqExp& x) = 0;

	virtual int visit(PairExpList& x) = 0;
	virtual int visit(LastExpList& x) = 0;
};

#endif
