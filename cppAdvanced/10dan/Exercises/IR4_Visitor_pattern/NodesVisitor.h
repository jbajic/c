#ifndef _NODES_VISITOR_H_
#define _NODES_VISITOR_H_

#include "Visitor.h"
#include "IR.h"

struct NodesVisitor : Visitor {
	int visit(CompoundStm& x) override {
		int n = x.stm1->accept(*this);
		n += x.stm2->accept(*this);
		return n + 1;
	}
	int visit(AssignStm& x) {
		return x.exp->accept(*this) + 1;
	}
	int visit(PrintStm& x) {
		return x.exps->accept(*this) + 1;
	}

	int visit(IdExp& x) override {
		return 1;
	}
	int visit(NumExp& x) override {
		return 1;
	}
	int visit(OpExp& x) override {
		int n = x.left->accept(*this);
		n += x.right->accept(*this);
		return n + 1;
	}
	int visit(EseqExp& x) override {
		int n = x.stm->accept(*this);
		n += x.exp->accept(*this);
		return n + 1;
	}

	int visit(PairExpList& x) override {
		int n = x.head->accept(*this);
		n += x.tail->accept(*this);
		return n + 1;
	}
	int visit(LastExpList& x) override {
		return x.exp->accept(*this) + 1;
	}
};

#endif
