#ifndef _ARGS_VISITOR_H_
#define _ARGS_VISITOR_H_

#include "Visitor.h"
#include "IR.h"

#include <algorithm>


struct ArgsVisitor : Visitor {
	int visit(CompoundStm& x) override {
		int n = x.stm1->accept(*this);
		int m = x.stm2->accept(*this);
		return std::max(n, m);
	}
	int visit(AssignStm& x) {
		return x.exp->accept(*this);
	}
	int visit(PrintStm& x) {
		return x.exps->accept(*this);
	}

	int visit(IdExp& x) override {
		return 1;
	}
	int visit(NumExp& x) override {
		return 1;
	}
	int visit(OpExp& x) override {
		int n = x.left->accept(*this);
		int m = x.right->accept(*this);
		return std::max(n, m);
	}
	int visit(EseqExp& x) override {
		int n = x.stm->accept(*this);
		int m = x.exp->accept(*this);
		return std::max(n, m);
	}

	int visit(PairExpList& x) override {
		int n = x.head->accept(*this);
		int m = x.tail->accept(*this) + 1;
		return std::max(n, m);
	}
	int visit(LastExpList& x) override {
		return x.exp->accept(*this);
	}
};

#endif
