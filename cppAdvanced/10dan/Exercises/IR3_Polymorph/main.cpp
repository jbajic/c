﻿/*
Interpretation of tree-like IR.
Program goes through the IR and searches for maximal number of arguments of print statement.

Version 3: Using inheritance and polymorphism: virtual function calls.
(getKind is not used)
*/

#include "IR.h"

#include <iostream>

using std::cout;


IrStm* prog1() {
	// a := 5+3; b := (print(a, a-1), 10*a); print(b);
	return
		makeCompoundStm(
			makeAssignStm("a", makeOpExp(makeNumExp(5), OK_PLUS, makeNumExp(3))),
			makeCompoundStm(
				makeAssignStm("b", makeEseqExp(
					makePrintStm(makePairExpList(makeIdExp("a"), makeLastExpList(
						makeOpExp(makeIdExp("a"), OK_MINUS, makeNumExp(1))))),
					makeOpExp(makeNumExp(10), OK_MUL, makeIdExp("a")))),
				makePrintStm(makeLastExpList(makeIdExp("b")))
			)
		);
}


int main()
{
	IrStm* root = prog1();
	cout << root->args();

	return 0;
}
