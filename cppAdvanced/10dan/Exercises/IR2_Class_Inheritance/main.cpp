﻿/*
Interpretation of tree-like IR.
Program goes through the IR and searches for maximal number of arguments of print statement.

Version 2: Using class inheritance (instead of unions). But without polymorphism.
*/

#include "IR.h"

#include <iostream>
#include <algorithm>

using std::cout;
using std::max;


IrStm* prog1() {
	// a := 5+3; b := (print(a, a-1), 10*a); print(b);
	return
		makeCompoundStm(
			makeAssignStm("a", makeOpExp(makeNumExp(5), OK_PLUS, makeNumExp(3))),
			makeCompoundStm(
				makeAssignStm("b", makeEseqExp(
					makePrintStm(makePairExpList(makeIdExp("a"), makeLastExpList(
						makeOpExp(makeIdExp("a"), OK_MINUS, makeNumExp(1))))),
					makeOpExp(makeNumExp(10), OK_MUL, makeIdExp("a")))),
				makePrintStm(makeLastExpList(makeIdExp("b")))
			)
		);
}


int stmargs(IrStm* x);
int expargs(IrExp* x);
int explistargs(IrExpList* x);


int stmargs(IrStm* x) {
	switch (x->getKind()) {
	case SK_COMPOUND: {
		int n = stmargs(static_cast<CompoundStm*>(x)->stm1);
		int m = stmargs(static_cast<CompoundStm*>(x)->stm2);
		return max(n, m);
	}
	case SK_ASSIGN:
		return expargs(static_cast<AssignStm*>(x)->exp);
	case SK_PRINT:
		return explistargs(static_cast<PrintStm*>(x)->exps);
	}
	return -1; // error
}


int expargs(IrExp* x) {
	switch (x->getKind()) {
	case EK_ID:
	case EK_NUM:
		return 1;
	case EK_OP: {
		int n = expargs(static_cast<OpExp*>(x)->left);
		int m = expargs(static_cast<OpExp*>(x)->right);
		return max(n, m);
	}
	case EK_ESEQ: {
		int n = stmargs(static_cast<EseqExp*>(x)->stm);
		int m = expargs(static_cast<EseqExp*>(x)->exp);
		return max(n, m);
	}
	}
	return -1; // error
}


int explistargs(IrExpList* x) {
	switch (x->getKind()) {
	case ELK_PAIR: {
		int n = expargs(static_cast<PairExpList*>(x)->head);
		int m = explistargs(static_cast<PairExpList*>(x)->tail) + 1;
		return max(n, m);
	}
	case ELK_LAST:
		return expargs(static_cast<LastExpList*>(x)->exp);
	}
	return -1; // error
}


int main()
{
	IrStm* root = prog1();
	cout << stmargs(root);

	return 0;
}
