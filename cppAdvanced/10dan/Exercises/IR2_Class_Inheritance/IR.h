#ifndef _IR_H_
#define _IR_H_


#include <string>


struct IrExpList;
struct IrExp;
struct IrStm;


////////////////
// Statements //
////////////////
enum StmKind {
	SK_COMPOUND,
	SK_ASSIGN,
	SK_PRINT
};


struct IrStm {
	IrStm(StmKind x) : kind(x) {}
	StmKind getKind() const { return kind; }
private:
	StmKind kind;
};


struct CompoundStm : IrStm {
	CompoundStm(IrStm* x, IrStm* y) : IrStm(SK_COMPOUND), stm1(x), stm2(y) {}
	IrStm* stm1;
	IrStm* stm2;
};


struct AssignStm : IrStm {
	AssignStm(std::string x, IrExp* y) : IrStm(SK_ASSIGN), id(x), exp(y) {}
	std::string id;
	IrExp* exp;
};


struct PrintStm : IrStm {
	PrintStm(IrExpList* x) : IrStm(SK_PRINT), exps(x) {}
	IrExpList* exps;
};


IrStm* makeCompoundStm(IrStm* x, IrStm* y) {
	return new CompoundStm(x, y);
}


IrStm* makeAssignStm(std::string x, IrExp* y) {
	return new AssignStm(x, y);
}


IrStm* makePrintStm(IrExpList* x) {
	return new PrintStm(x);
}


/////////////////
// Expressions //
/////////////////
enum ExpKind {
	EK_ID,
	EK_NUM,
	EK_OP,
	EK_ESEQ
};


struct IrExp {
	IrExp(ExpKind x) : kind(x) {}
	ExpKind getKind() const { return kind; }
private:
	ExpKind kind;
};


enum OpKind {
	OK_PLUS,
	OK_MINUS,
	OK_MUL,
	OK_DIV
};


struct IdExp : IrExp {
	IdExp(const std::string& x) : IrExp(EK_ID), name(x) {}
	std::string name;
};


struct NumExp : IrExp {
	NumExp(int x) : IrExp(EK_NUM), value(x) {}
	int value;
};


struct OpExp : IrExp {
	OpExp(IrExp* x, OpKind op, IrExp* y) : IrExp(EK_OP), left(x), operKind(op), right(y) {}
	IrExp* left;
	OpKind operKind;
	IrExp* right;
};


struct EseqExp : IrExp {
	EseqExp(IrStm* x, IrExp* y) : IrExp(EK_ESEQ), stm(x), exp(y) {}
	IrStm* stm;
	IrExp* exp;
};


IrExp* makeIdExp(const std::string& x) {
	return new IdExp(x);
}


IrExp* makeNumExp(int x) {
	return new NumExp(x);
}


IrExp* makeOpExp(IrExp* x, OpKind op, IrExp* y) {
	return new OpExp(x, op, y);
}


IrExp* makeEseqExp(IrStm* x, IrExp* y) {
	return new EseqExp(x, y);
}


//////////////////////
// Expressions list //
//////////////////////
enum ExpListKind {
	ELK_PAIR,
	ELK_LAST
};

struct IrExpList {
	IrExpList(ExpListKind x) : kind(x) {}
	ExpListKind getKind() const { return kind; }
private:
	ExpListKind kind;
};


struct PairExpList : IrExpList {
	PairExpList(IrExp* x, IrExpList* y) : IrExpList(ELK_PAIR), head(x), tail(y) {}
	IrExp* head;
	IrExpList* tail;
};


struct LastExpList : IrExpList {
	LastExpList(IrExp* x) : IrExpList(ELK_LAST), exp(x) {}
	IrExp* exp;
};


IrExpList* makePairExpList(IrExp* x, IrExpList* y) {
	return new PairExpList(x, y);
}


IrExpList* makeLastExpList(IrExp* x) {
	return new LastExpList(x);
}


#endif
