﻿/*
Interpretation of tree-like IR.
Program goes through the IR and searches for maximal number of arguments of print statement.

Version 5: Using std::variant. In variant (in contrast to union) std::string can be used.
Also, std::visit is used, a function that supports usage of visitor pattern on variants.
Additional visitor is provided, which counts all the nodes in the tree.
*/

#include "IR.h"
#include "ArgsVisitor.h"
#include "NodesVisitor.h"

#include <iostream>
#include <variant>

using std::cout;
using std::endl;
using std::visit;


IrStm* prog1() {
	// a := 5+3; b := (print(a, a-1), 10*a); print(b);
	return
		makeCompoundStm(
			makeAssignStm("a", makeOpExp(makeNumExp(5), OK_PLUS, makeNumExp(3))),
			makeCompoundStm(
				makeAssignStm("b", makeEseqExp(
					makePrintStm(makePairExpList(makeIdExp("a"), makeLastExpList(
						makeOpExp(makeIdExp("a"), OK_MINUS, makeNumExp(1))))),
					makeOpExp(makeNumExp(10), OK_MUL, makeIdExp("a")))),
				makePrintStm(makeLastExpList(makeIdExp("b")))
			)
		);
}


template<typename VisitorT>
int interpret(IrStm* s, VisitorT v) {
    return visit(v, *s);
}

int main()
{
	IrStm* root = prog1();
	cout << interpret(root, ArgsVisitor()) << endl;
	cout << interpret(root, NodesVisitor());

	return 0;
}
