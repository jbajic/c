#ifndef _NODES_VISITOR_H_
#define _NODES_VISITOR_H_

#include "IR.h"

#include <variant>


struct NodesVisitor
{
    int operator()(CompoundStm& x) {
        int n = std::visit(*this, *(x.stm1));
        n += std::visit(*this, *(x.stm2));
        return n + 1;
    }
    int operator()(AssignStm& x) {
        return std::visit(*this, *(x.exp)) + 1;
    }
    int operator()(PrintStm& x) {
        return std::visit(*this, *(x.exps)) + 1;
    }
    int operator()(IdExp& x [[maybe_unused]]) {
        return 1;
    }
    int operator()(NumExp& x [[maybe_unused]]) {
        return 1;
    }
    int operator()(OpExp& x) {
        int n = std::visit(*this, *(x.left));
        n += std::visit(*this, *(x.right));
        return n + 1;
    }
    int operator()(EseqExp& x) {
        int n = std::visit(*this, *(x.stm));
        n += std::visit(*this, *(x.exp));
        return n + 1;
    }
    int operator()(PairExpList& x) {
        int n = std::visit(*this, *(x.head));
        n += std::visit(*this, *(x.tail));
        return n + 1;
    }
    int operator()(LastExpList& x) {
    	return std::visit(*this, *(x.last)) + 1;
    }
};


#endif
