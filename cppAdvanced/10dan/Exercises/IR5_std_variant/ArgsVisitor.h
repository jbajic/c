#ifndef _ARGS_VISITOR_H_
#define _ARGS_VISITOR_H_

#include "IR.h"

#include <variant>
#include <algorithm>


struct ArgsVisitor
{
    int operator()(CompoundStm& x) {
        int n = std::visit(ArgsVisitor(), *(x.stm1));
        int m = std::visit(*this, *(x.stm2));
        return std::max(n, m);
    }
    int operator()(AssignStm& x) {
        return std::visit(*this, *(x.exp));
    }
    int operator()(PrintStm& x) {
        return std::visit(*this, *(x.exps));
    }
    int operator()(IdExp& x [[maybe_unused]]) {
        return 1;
    }
    int operator()(NumExp& x [[maybe_unused]]) {
        return 1;
    }
    int operator()(OpExp& x) {
        int n = std::visit(*this, *(x.left));
        int m = std::visit(*this, *(x.right));
        return std::max(n, m);
    }
    int operator()(EseqExp& x) {
        int n = std::visit(*this, *(x.stm));
        int m = std::visit(*this, *(x.exp));
        return std::max(n, m);
    }
    int operator()(PairExpList& x) {
        int n = std::visit(*this, *(x.head));
        int m = std::visit(*this, *(x.tail)) + 1;
        return std::max(n, m);
    }
    int operator()(LastExpList& x) {
    	return std::visit(*this, *(x.last));
    }
};

#endif
