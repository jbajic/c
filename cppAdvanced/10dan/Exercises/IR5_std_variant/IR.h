#ifndef _IR_H_
#define _IR_H_

#include "Visitor.h"

#include <variant>
#include <string>


struct CompoundStm;
struct AssignStm;
struct PrintStm;
using IrStm = std::variant<AssignStm, CompoundStm, PrintStm>;

struct IdExp;
struct NumExp;
struct OpExp;
struct EseqExp;
using IrExp = std::variant<IdExp, NumExp, OpExp, EseqExp>;
	
struct PairExpList;
struct LastExpList;
using IrExpList = std::variant<PairExpList, LastExpList>;


////////////////
// Statements //
////////////////
struct CompoundStm {
	CompoundStm(IrStm* s1, IrStm* s2) : stm1(s1), stm2(s2) {}
	IrStm* stm1;
	IrStm* stm2;
};


struct AssignStm {
	AssignStm(std::string i, IrExp* e) : id(i), exp(e) {}
	std::string id;
	IrExp* exp;
};


struct PrintStm {
	PrintStm(IrExpList* el) : exps(el) {}
	IrExpList* exps;
};


IrStm* makeCompoundStm(IrStm* stm1, IrStm* stm2) {
	return new IrStm(std::in_place_type<CompoundStm>, stm1, stm2);
}


IrStm* makeAssignStm(string id, IrExp* exp) {
	return new IrStm(std::in_place_type<AssignStm>, id, exp);
}


IrStm* makePrintStm(ExpList* exps) {
	return new IrStm(std::in_place_type<PrintStm>, exps);
}


/////////////////
// Expressions //
/////////////////
struct IdExp {
	IdExp(string i) : id(i) {}
	std::string id;
};


struct NumExp {
	NumExp(int n) : num(n) {}
	int num;
};


enum OpKind {
	OK_PLUS,
	OK_MINUS,
	OK_MUL,
	OK_DIV
};
struct OpExp {
	OpExp(IrExp* l, OpKind o, IrExp* r)
		: left(l), oper(o), right(r) {}
	IrExp* left;
	OpKind oper;
	IrExp* right;
};


struct EseqExp {
	EseqExp(IrStm* s, IrExp* e)
		: stm(s), exp(e) {}
	IrStm* stm;
	IrExp* exp;
};


IrExp* makeIdExp(string id) {
    return new IrExp(std::in_place_type<IdExp>, id);
}


IrExp* makeNumExp(int num) {
    return new IrExp(std::in_place_type<NumExp>, num);
}


IrExp* makeOpExp(IrExp* left, BinOpKind oper, IrExp* right) {
    return new IrExp(std::in_place_type<OpExp>, left, oper, right);
}


IrExp* makeEseqExp(IrStm* stm, IrExp* exp) {
    return new IrExp(std::in_place_type<EseqExp>, stm, exp);
}


//////////////////////
// Expressions list //
//////////////////////
struct PairExpList {
	PairExpList(IrExp* h, IrExpList* t) : head(h), tail(t) {}
	IrExp* head;
	IrExpList* tail;
};


struct LastExpList {
	LastExpList(IrExp* l) : last(l) {}
	IrExp* last;
};


IrExpList* makePairExpList(IrExp* head, IrExpList* tail) {
	return new IrExpList(std::in_place_type<PairExpList>, head, tail);
}


IrExpList* makeLastExpList(IrExp* last) {
	return new IrExpList(std::in_place_type<LastExpList>, last);
}


#endif
