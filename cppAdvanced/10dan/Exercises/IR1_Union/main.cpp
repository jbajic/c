﻿/*
Interpretation of tree-like IR.
Program goes through the IR and searches for maximal number of arguments of print statement.

Version 1: Using unions and free functions. Unions can not contain std::string, and similar classes,
so type "string" is used, which is just another name for fixed length char array.
*/

#include "IR.h"

#include <iostream>
#include <algorithm>

using std::cout;
using std::max;


IrStm* prog1() {
	// a := 5+3; b := (print(a, a-1), 10*a); print(b);
	return
		makeCompoundStm(
			makeAssignStm("a", makeOpExp(makeNumExp(5), OK_PLUS, makeNumExp(3))),
			makeCompoundStm(
				makeAssignStm("b", makeEseqExp(
					makePrintStm(makePairExpList(makeIdExp("a"), makeLastExpList(
						makeOpExp(makeIdExp("a"), OK_MINUS, makeNumExp(1))))),
					makeOpExp(makeNumExp(10), OK_MUL, makeIdExp("a")))),
				makePrintStm(makeLastExpList(makeIdExp("b")))
			)
		);
}


int stmargs(IrStm* x);
int expargs(IrExp* x);
int explistargs(IrExpList* x);


int stmargs(IrStm* x) {
	switch (x->kind) {
	case SK_COMPOUND: {
		int n = stmargs(x->u.compound.stm1);
		int m = stmargs(x->u.compound.stm2);
		return max(n, m);
	}
	case SK_ASSIGN:
		return expargs(x->u.assign.exp);
	case SK_PRINT:
		return explistargs(x->u.print.exps);
	}
	return -1; // error
}


int expargs(IrExp* x) {
	switch (x->kind) {
	case EK_ID:
	case EK_NUM:
		return 1;
	case EK_OP: {
		int n = expargs(x->u.op.left);
		int m = expargs(x->u.op.right);
		return max(n, m);
	}
	case EK_ESEQ: {
		int n = stmargs(x->u.eseq.stm);
		int m = expargs(x->u.eseq.exp);
		return max(n, m);
	}
	}
	return -1; // error
}


int explistargs(IrExpList* x) {
	switch (x->kind) {
	case ELK_PAIR: {
		int n = expargs(x->u.pair.head);
		int m = explistargs(x->u.pair.tail) + 1;
		return max(n, m);
	}
	case ELK_LAST:
		return expargs(x->u.last);
	}
	return -1; // error
}


int main()
{
	IrStm* root = prog1();
	cout << stmargs(root);

	return 0;
}
