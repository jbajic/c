#ifndef _IR_H_
#define _IR_H_

#include <string.h>

typedef char string[64];

struct IrExpList;
struct IrExp;
struct IrStm;


////////////////
// Statements //
////////////////
enum StmKind {
	SK_COMPOUND,
	SK_ASSIGN,
	SK_PRINT
};


struct CompoundStm {
	IrStm* stm1;
	IrStm* stm2;
};


struct AssignStm {
	string id;
	IrExp* exp;
};


struct PrintStm {
	IrExpList* exps;
};


struct IrStm {
	StmKind kind;
	union {
		CompoundStm compound;
		AssignStm assign;
		PrintStm print;
	} u;
};


IrStm* makeCompoundStm(IrStm* x, IrStm* y) {
	IrStm* res = new IrStm();
	res->kind = SK_COMPOUND;
	res->u.compound.stm1 = x;
	res->u.compound.stm2 = y;
	return res;
}


IrStm* makeAssignStm(string x, IrExp* y) {
	IrStm* res = new IrStm();
	res->kind = SK_ASSIGN;
	strncpy(res->u.assign.id, x, 63);
	res->u.assign.exp = y;
	return res;
}


IrStm* makePrintStm(IrExpList* x) {
	IrStm* res = new IrStm();
	res->kind = SK_PRINT;
	res->u.print.exps = x;
	return res;
}


/////////////////
// Expressions //
/////////////////
enum ExpKind {
	EK_ID,
	EK_NUM,
	EK_OP,
	EK_ESEQ
};


enum OpKind {
	OK_PLUS,
	OK_MINUS,
	OK_MUL,
	OK_DIV
};


struct OpExp {
	OpKind operKind;
	IrExp* left;
	IrExp* right;
};


struct EseqExp {
	IrStm* stm;
	IrExp* exp;
};


struct IrExp {
	ExpKind kind;
	union {
		string id;
		int num;
		OpExp op;
		EseqExp eseq;
	} u;
};


IrExp* makeIdExp(string x) {
	IrExp* res = new IrExp();
	res->kind = EK_ID;
	strncpy(res->u.id, x, 63);
	return res;
}


IrExp* makeNumExp(int x) {
	IrExp* res = new IrExp();
	res->kind = EK_NUM;
	res->u.num = x;
	return res;
}


IrExp* makeOpExp(IrExp* x, OpKind op, IrExp* y) {
	IrExp* res = new IrExp();
	res->kind = EK_OP;
	res->u.op.left = x;
	res->u.op.operKind = op;
	res->u.op.right = y;
	return res;
}


IrExp* makeEseqExp(IrStm* x, IrExp* y) {
	IrExp* res = new IrExp();
	res->kind = EK_ESEQ;
	res->u.eseq.stm = x;
	res->u.eseq.exp = y;
	return res;
}


//////////////////////
// Expressions list //
//////////////////////
enum ExpListKind {
	ELK_PAIR,
	ELK_LAST
};


struct PairExpList {
	IrExp* head;
	IrExpList* tail;
};


struct IrExpList {
	ExpListKind kind;
	union {
		PairExpList pair;
		IrExp* last;
	} u;
};


IrExpList* makePairExpList(IrExp* x, IrExpList* y) {
	IrExpList* res = new IrExpList();
	res->kind = ELK_PAIR;
	res->u.pair.head = x;
	res->u.pair.tail = y;
	return res;
}


IrExpList* makeLastExpList(IrExp* x) {
	IrExpList* res = new IrExpList();
	res->kind = ELK_LAST;
	res->u.last = x;
	return res;
}


#endif
