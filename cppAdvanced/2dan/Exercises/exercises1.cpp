﻿#include "MyBigInt.hpp"

#include <iostream>
// Way too much time
// MyBigInt fib(MyBigInt n) 
// { 
//     if ((n == 1) || (n == 0))
//         return n; 
//     return fib(n-1) + fib(n-2); 
// }

MyBigInt fib(int n) 
{ 
  /* Declare an array to store Fibonacci numbers. */
  MyBigInt f[n+2];   // 1 extra to handle case, n = 0 
  int i; 
  
  /* 0th and 1st number of the series are 0 and 1*/
  f[0] = 0; 
  f[1] = 1; 
  
  for (i = 2; i <= n; i++) 
  { 
      /* Add the previous 2 numbers in the series 
         and store it */
      f[i] = f[i-1] + f[i-2]; 
  } 
  
  return f[n]; 
} 

int main()
{
    MyBigInt k(7);
    cout << k << endl;

    k = fib(7);
    cout << k << endl;
    k = k + 5;
    std::cout << k << std::endl;
    k = 7 + fib(11);
    std::cout << k << std::endl;
    k = 13_mbi;
    std::cout << k << std::endl;
    MyBigInt l = fib(300);
    cout << l << endl;
    if (l != 222232244629420445529739893461909967206666939096499764990979600_mbi) {
        std::cout << "Error!" << std::endl;
        return 1;
    }
    std::cout << l << std::endl;
    return 0;
}
