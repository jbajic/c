#ifndef _MY_BIG_INT_
#define _MY_BIG_INT_

#include <cstdint>
#include <cstring>
#include <array>
#include <iostream>

using namespace std;

struct MyBigInt {

private:
    static constexpr int size{70};
    std::array<int, size> digits;
public:
    MyBigInt(): MyBigInt(0) {}

    MyBigInt(int k) {
        for (int i = size - 1; i >= 0; i--) {
            digits[i] = k % 10;
            k = k / 10;
        }
    }

    MyBigInt(const char *literal) {
        int stringSize{std::strlen(literal) - 1};
        int i{0};
        for (; stringSize >= i; i++) {
            digits[size - i - 1] = literal[stringSize - i] - 48;
        }
        for (; i < size; i++) {
            digits[size - i - 1] = 0;
        }
    }

    MyBigInt(const MyBigInt &rhs) {
        for (int i = 0; i < size; i++) {
            digits[i] = rhs.digits[i];
        }
    }

    MyBigInt &operator=(const MyBigInt &rhs) {
        for (int i = 0; i < size; i++) {
            digits[i] = rhs.digits[i];
        }
        return *this;
    }

    MyBigInt operator+(const MyBigInt &rhs) {
        MyBigInt newNum;
        int carrryOn = 0;
        for (int i = size - 1; i >= 0; --i) {
            int number = digits[i] + rhs.digits[i] + carrryOn;
            if (number >= 10) {
                newNum.digits[i] = number % 10;
                carrryOn = 1;
            } else {
                newNum.digits[i] = number;
                carrryOn = 0;
            }
        }
        return newNum;
    }

    MyBigInt operator+(const int &rhs) {
        return *this + MyBigInt(rhs);
    }

    MyBigInt operator-(const MyBigInt &rhs) {
        int carrryOn = 0;
        MyBigInt newNum;
        for (int i = size - 1; i >= 0; --i) {
            if (digits[i] >= (rhs.digits[i] + carrryOn)) {
                newNum.digits[i] = digits[i] - rhs.digits[i] - carrryOn;
                carrryOn = 0;
            } else {
                newNum.digits[i] = (digits[i] + 10) - rhs.digits[i] - carrryOn;
                carrryOn = 1;
            }
        }
        return newNum;
    }

    MyBigInt operator-(const int &rhs) {
        return *this - MyBigInt(rhs);
    }

    bool operator!=(const MyBigInt &rhs) {
        for (int i = 0; i < size; i++) {
            if (digits[i] != rhs.digits[i])
                return true;
        }
        return false;
    }

    bool operator==(const MyBigInt &rhs) {
        for (int i = 0; i < size; i++) {
            if (digits[i] != rhs.digits[i])
                return false;
        }
        return true;
    }

    bool operator==(const int &rhs) {
        return *this == MyBigInt(rhs);
    }

    friend std::ostream &operator<<(std::ostream &os, const MyBigInt &dt);

    friend MyBigInt operator+(const int &lhs, const MyBigInt &rhs);

};

std::ostream &operator<<(std::ostream &os, const MyBigInt &dt) {
    for (int i = 0; i < dt.size; i++) {
        os << dt.digits[i] << ", ";
    }
    os << std::endl;
    return os;
}

MyBigInt operator+(const int &lhs, const MyBigInt &rhs) {
    return MyBigInt(lhs) + rhs;
}

//MyBigInt operator "" _mbi(unsigned long long rhs) {
//    return MyBigInt(static_cast<int>(rhs));
//}

//MyBigInt operator "" _mbi(unsigned long long rhs) {
//    return MyBigInt(static_cast<int>(rhs));
//}

MyBigInt operator "" _mbi(const char *rhs) {
    return MyBigInt(rhs);
}

#endif
