#include <iostream>
#include <memory>

using namespace std;


template<typename ReturnValue, typename... Args>
class Function<ReturnValue(Args...)> {
public:
    template<typename T>
    Function(T t) {
        callable = make_unique<Callable<T>>(t);
    }

    template<typename T>
    Function& operator=(T t) {
        callable = make_unique<Callable<T>>(t);
        return *this;
    }

    ReturnValue operator()(Ts... args) -> int {
        return callback->Invoke(args...);
    }

private:
    template<typename T>
    class Callable{
    public:
        Callable(const T& t): callback{t} {}
        ReturnValue Invoke(Args... args) {
            return callback(args...);
        }
    private:
        T callback;
    };
    unique_ptr<Callable> callable;
};

int foo(int a) {
    return x + 1;
}

struct fooS {
    fooS(int a): m_a{a} {}

    int operator()(int x) {
        return  x + m_a;
    }

private:
    int m_a{0};
};

int main() {
    Function<int(int)> f1(foo);
    Function<int(int)> f2(fooS);

//    std << f1(7) << " " << f2(8) << endl;
//
//    std::array<Function<int(int)>, 2> ary;
//    ary[0] = f1;
//    ary[1] = f2;
//
//    cout << arr[0][7] << " " << ary[1](8) << endl;
}