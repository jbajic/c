// testc11.cpp: This program should compile cleanly on a C++11 platform.

#include <iostream>
using namespace std;

struct Sum
{
    template<typename T>
            static T sum(T n)
    {
        return n;
    }

    template<typename T, typename... Args>
            static auto sum(T n, Args... rest) -> decltype(n + sum(rest...))
    {
        return n + sum(rest...);
    }
};

template<typename T, typename... Args>
auto sum(T n, Args... rest) -> decltype(n + Sum::sum(rest...))
{
    return n + Sum::sum(rest...);
}

template<typename... Args>
auto avg(Args... args) -> decltype(Sum::sum(args...))
{
    return sum(args...) / (sizeof... (args));
}


int main()
{
    cout << "Testing the file scope version of sum():\n";
    cout << "sum(1, 2.2) = " << sum(1, 2.2) << endl;

    cout << "sum(1,2,3,4,5,6,7) = " <<  sum(1,2,3,4,5,6,7) << endl;
    cout << "sum(3.14, 2.718, 2.23606) = " << sum(3.14, 2.718, 2.23606) << endl;

    cout << "sum(1,2,3) = " << sum(1,2,3) << endl;
    cout << "avg(2.2, 3.3, 4.4) = " << avg(2.2, 3.3, 4.4) << endl;

    cout << "Different types:\n";
    cout << "avg(2.2, 3, 4.4, 5L) = " << avg(2.2, 3, 4.4, 5L) << endl;

}

