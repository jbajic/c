#include <iostream>

using namespace std;

class BigInt {
public:
    int *arr;
    int size;

    BigInt() : size{0}, arr{nullptr} {}

    BigInt(int size) : size{size}, arr{new int[size]} {
        cout << "Constructor" << endl;
    }

    ~BigInt() {
        cout << "Descructor" << endl;
        delete[] arr;
        size = 0;
    }

    BigInt(const BigInt &other) {
        cout << "Copy constructor" << endl;
        size = other.size;
        arr = new int[size];
        for (int i = 0; i < size; ++i) {
            arr[i] = other.arr[i];
        }
    }

    BigInt &operator=(const BigInt &other) {
        cout << "Copy assigment constructor" << endl;
        delete[] arr;
        size = other.size;
        arr = new int[size];
        for (int i = 0; i < size; ++i) {
            arr[i] = other.arr[i];
        }
        return *this;
    }

    BigInt(BigInt &&other) {
        cout << "Move constructor" << endl;

        arr = other.arr;
        size = other.size;
        other.arr = nullptr;
        other.size = 0;
    }

    BigInt &operator=(BigInt &&other) {
        cout << "Move assigment" << endl;
        if (*this == other)
            return *this;
        arr = other.arr;
        size = other.size;
        other.arr = nullptr;
        other.size = 0;
    }

    bool operator==(const BigInt &other) {
        return arr == other.arr;
    }

    BigInt operator+(const BigInt &other) {
        BigInt result(size);
        for(int i = 0; i < 5; ++i) {
            result.arr[i] = arr[i] + other.arr[i];
        }
        return result;
    }

};

BigInt foo() {
    return BigInt(21);
}

int main() {

    constexpr int size_ = 41;

    BigInt ladida(size_);
    for (int i = 0; i < size_; i++) {
        ladida.arr[i] = i + 1;
    }
    BigInt ladida2 = ladida;
    BigInt ladida3(3);
    ladida3 = ladida2;
    BigInt ladida4 = foo();
    //ladida = foo();
    BigInt ladida32 = ladida2 + ladida2 ;
    return 0;
}

