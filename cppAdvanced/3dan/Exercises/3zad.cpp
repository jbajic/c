#include "3zad.hpp"

using namespace std;

int main() {
    int a = 3;
    int& r = a;
    int && rr = 5;
    const int ca = 6;
    const int& cr = ca;
//    foo(5);
    foo(a);
    foo(r);
    foo(rr);
    foo(ca);
    foo(cr);
    return 0;
}