#include <iostream>
#include "type_name.hpp"

using namespace std;

template<typename T>
void foo(T x) {
    cout << type_name<T>() << endl;
}

template<typename T>
void foo(T& x) {
    cout << type_name<T>() << endl;
}

template<typename T>
void foo(const T& x) {
    cout << type_name<T>() << endl;
}

template<typename T>
void foo(T&& x) {
    cout << type_name<T>() << endl;
}