#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <future>

constexpr int cut_off = 10000;

template<typename Iterator, typename F>
auto pprocess(Iterator begin, Iterator end, F &&f){
    using value_type = typename std::iterator_traits<Iterator>::value_type;
    auto func{std::forward<F>(f)};
    auto size = std::distance(begin, end);

    if (size <= cut_off){
        return func(begin, end);
    }else{
        int thread_count = std::thread::hardware_concurrency();
//        std::vector<std::thread> threads;
        std::vector<std::future<value_type>> futures;
        std::vector<value_type> mins(thread_count);
        //std::cout << "thread_count= " << thread_count << std::endl;

        auto first = begin;
        auto last = first;
        size /= thread_count;

        for (int i = 0; i < thread_count; ++i){
            first = last;
            if (i == thread_count -1){
                last = end;
            }else{
                std::advance(last, size);
            }
//            threads.emplace_back([first, last, func, &r=mins[i]](){
//                                 r = func(first, last);
//                                 });
            futures.emplace_back(std::async(std::launch::async, [first, last, func](){
                return func(first, last);
             }));
        }
//        for(auto &t : threads){
        for(auto &t : futures){
//            t.join();
            mins.emplace_back(t.get());
        }
        return func(mins.begin(), mins.end());
    }
}


template<typename Iterator>
auto pmin(Iterator begin, Iterator end){
    return pprocess(begin, end,
                    [](auto b, auto e){return *std::min_element(b, e);});
}

template<typename Iterator>
auto pmax(Iterator begin, Iterator end){
    return pprocess(begin, end,
                    [](auto b, auto e){return *std::max_element(b, e);});
}

int main(){
    std::vector<int> vi(100000, 0);
    vi.insert(vi.end(), {-1, 0, 1, 0, -2, 0, 2, 0, -3, 0, 3, 0});
    

    auto min = pmin(std::begin(vi), std::end(vi));
    std::cout << "min= " << min << std::endl;

    auto max = pmax(std::begin(vi), std::end(vi));
    std::cout << "max= " << max << std::endl;

    return 0;
}
