#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <future>
#include <numeric>
#include <random>
#include <iomanip>

constexpr int cut_off = 10000;

template<typename Iterator, typename F>
auto pprocess(Iterator begin, Iterator end, F &&f){
    using value_type = typename std::iterator_traits<Iterator>::value_type;
    auto func{std::forward<F>(f)};
    auto size = std::distance(begin, end);

    if (size <= cut_off){
        return func(begin, end);
    }else{
        int thread_count = std::thread::hardware_concurrency();
//        std::vector<std::thread> threads;
        std::vector<std::future<value_type>> futures;
        std::vector<value_type> sums(thread_count);
        //std::cout << "thread_count= " << thread_count << std::endl;

        auto first = begin;
        auto last = first;
        size /= thread_count;

        for (int i = 0; i < thread_count; ++i){
            first = last;
            if (i == thread_count -1){
                last = end;
            }else{
                std::advance(last, size);
            }
//            threads.emplace_back([first, last, func, &r=mins[i]](){
//                                 r = func(first, last);
//                                 });
            futures.emplace_back(std::async(std::launch::async, [first, last, func](){
                return func(first, last);
            }));
        }
//        for(auto &t : threads){
        for(auto &t : futures){
//            t.join();
            sums.emplace_back(t.get());
        }
        return func(sums.begin(), sums.end());
    }
}

template<typename Iterator>
auto psum(Iterator begin, Iterator end){
    return pprocess(begin, end,
                    [](auto b, auto e){return std::accumulate(b, e, 0);});
}

int main(){
    std::vector<int> vi(100000, 0);
    std::vector<int> test(100000);

    vi.insert(vi.end(), {-1, 0, 1, 0, -2, 0, 2, 0, -3, 0, 3, 0});

    auto sum = psum(std::begin(vi), std::end(vi));
    std::cout << "sum= " << sum << std::endl;

    // Seed with a real random value, if available
    std::random_device r;

    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::normal_distribution<int> uniform_dist(1, 100);
    int mean = uniform_dist(e1);
    std::cout << "Randomly-chosen mean: " << mean << '\n';

    // Generate a normal distribution around that mean
    std::seed_seq seed2{r(), r(), r(), r(), r(), r(), r(), r()};
    std::mt19937 e2(seed2);
    std::normal_distribution<> normal_dist(mean, 2);

    for(int i = 0; i < 100000; ++i) {
        test[i].push_back(static_cast<int(>std::round(normal_dist(e2))));
    }
    auto sum2 = psum(std::begin(test), std::end(test));
    std::cout << "sum= " << sum2 << std::endl;


    return 0;
}
