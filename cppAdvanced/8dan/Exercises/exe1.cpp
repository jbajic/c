#include <iostream>

using namespace std;

template<typename T, typename... Args>
auto sum(T x, Args... args) {
    if constexpr (sizeof...(args) == 0)
        return x;
    else return x + sum(args...);
}

int main() {
    cout << sum(1, 2, 3, 4, 5, 6.9) << endl;
    return 0;
}