#include <iostream>

using namespace std;

template<typename T, typename... Args>
auto minimum(T x, Args... args) {
    if constexpr (sizeof...(args) == 0)
        return x;
    else {
        if (x < minimum(args...))  {
            return x;
        } else {
            return minimum(args...);
        }
    }
}

int main() {
    cout << minimum(1, 2, 3, 4, 5, 7, 9, 0, -6) << endl;
    return 0;
}