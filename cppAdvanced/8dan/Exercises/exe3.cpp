#include <iostream>
#include <type_traits>
#include <string>

using namespace std;

int my_printf(const char* format) {
    cout << format << endl;
}

template<typename T, typename... Args>
int my_printf(const char* format, T arg, Args... args) {
    int i{0};
    bool formatFound = false;
    for (i = 0; format[i] != '\0'; ++i) {
        if (format[i] == '%') {
            if(format[i + 1] == 'd') {
                if (!is_integral<T>::value) {
                    cerr << "Wrong nubmer format";
                    return -1;
                }
                formatFound = true;
                cout << arg;
                break;
            } else if(format[i + 1] == 'f') {
                if (!is_floating_point<T>::value) {
                    cerr << "Wrong nubmer format";
                    return -1;
                }
                formatFound = true;
                cout << arg;
                break;
            } else if(format[i + 1] == 's') {
                formatFound = true;
                cout << arg;
                break;
            } else {
                cerr << "Unknown format!";
                return -1;
            }
        } else {
            cout << format[i];
        }
    }
    if (formatFound) {
        my_printf(&format[i + 2], args...);
    } else {
        cout << endl;
    }
    return 0;
}

int main() {
    my_printf("sdfjsdfpQdfpmk");
    my_printf("Ladida1: %d", 4);
    my_printf("Ladida2: %d\n%d %d %f", 4,3,2,3.7676);
    return 0;
}