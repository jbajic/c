#include <iostream>
#include <stack>

bool isBracketClosing(const char element) {
    switch (element)
    {
    case ')':
    case '}':
    case ']':
        return true;
    default:
        return false;
    }
}

bool isBracketOpening(const char element) {
    switch (element)
    {
    case '(':
    case '{':
    case '[':
        return true;
    default:
        return false;
    }
}

bool isBracketClosed(const char last, const char current) {
    switch (last)
    {
    case '(':
        return current == ')';
    case '{':
        return current == '}';
    case '[':
        return current == ']';
    default:
        return false;
    }
}

bool isMathEqCorrect(const std::string& eq) {
    std::stack<char> brackets;
    for(const char elem:eq) {
        if(isBracketOpening(elem)) {
            brackets.push(elem);
        } else if(isBracketClosing(elem)) {
            if(!isBracketClosed(brackets.top(), elem)) {
                return false;
            }
            brackets.pop();
        }
    }
    if(brackets.size() == 0)
        return true;
    return false;
}

int main() {
  std::string str1{"([]{}){[(])}"};
  if(isMathEqCorrect(str1)) {
    std::cout << "Correct!" << std::endl;
  } else {
    std::cout << "Not correct!" << std::endl;
  }
  return 0;
}
