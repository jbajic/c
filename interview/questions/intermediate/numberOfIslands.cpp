#include <iostream>
#include <vector>
#include <string>
#include <list>
// [
//     w,w,w,w,w,l
//     w,w,l,l,w,l
//     w,w,w,l,l,l
//     w,w,w,w,w,w
//     w,w,l,w,l,w
//     w,w,w,l,l,w
// ]
using namespace std::string_literals;

void convertIsland(std::vector<std::string>& map, int i, int j) {
    if(i >= 0 && j >= 0 && i < map.size() && j < map[i].size() && map[i][j] == 'l') {
        map[i][j] = 'w';
        convertIsland(map, i + 1, j);
        convertIsland(map, i - 1, j);
        convertIsland(map, i, j + 1);
        convertIsland(map, i, j - 1);
    }
}

int numberOfIslands(std::vector<std::string>& map) {
    int counter{0};
    for(int i{0}; i < map.size(); ++i) {
        for(int j{0}; j < map[i].size(); ++j) {
            if(map[i][j] == 'l') {
                convertIsland(map, i, j);
                counter++;
            }
        }
    }
    return counter;
}

void visitIslandDFS(const std::vector<std::string>& map, std::vector<std::vector<bool>>& visitedMap, int i, int j) {
    std::list<std::pair<int,int>> coordinates;
    coordinates.push_back(std::make_pair(i, j));
    while(!coordinates.empty()) {
        for(std::list<std::pair<int,int>>::iterator it = coordinates.begin(); it != coordinates.end(); ++it) {
            if(it->first >= 0 && it->second >= 0 && it->first < map.size() && it->second < map[it->first].size()) {
                if(!visitedMap[it->first][it->second] && map[it->first][it->second] == 'l') {
                    coordinates.push_front(std::make_pair(it->first + 1, it->second));
                    coordinates.push_front(std::make_pair(it->first - 1, it->second));
                    coordinates.push_front(std::make_pair(it->first, it->second + 1));
                    coordinates.push_front(std::make_pair(it->first, it->second - 1));
                }
                visitedMap[it->first][it->second] = true;
            }
            coordinates.erase(it--);
        }
    }
}

int countNumberOfIslandsDFS(const std::vector<std::string>& map) {
    std::vector<std::vector<bool>> visitedMap(map.size(), std::vector<bool>(map[0].size(), false));
    int counter{0};
    for(int i{0}; i < map.size(); ++i) {
        for(int j{0}; j < map[i].size(); ++j) {
            if(map[i][j] == 'l' && !visitedMap[i][j]) {
                visitIslandDFS(map, visitedMap, i, j);
                counter++;
            }
        }
    }
    return counter;
}

int main() {
    std::vector<std::string> map {
        "wwwwwl"s,
        "wwllwl"s,
        "wwwlll"s,
        "wwwwww"s,
        "wwlwlw"s,
        "wwwllw"s
    };
    // Change input, recursive
    // std::cout << numberOfIslands(map) << std::endl;
    // Doesnt change input, uses extra memory, iterative
    std::cout << "Number of islands: " << countNumberOfIslandsDFS(map) << std::endl;
    return 0;
}
