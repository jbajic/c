#include <iostream>
#include <memory>

struct Data{
    int data;
    std::unique_ptr<Data> next;
    Data(int data): data{data}, next{nullptr} {}
};

class LinkedList{
private:
    std::unique_ptr<Data> head;
public:
    LinkedList(): head{nullptr} {}
    void push(int data) {
        std::unique_ptr<Data> temp{std::make_unique<Data>(data)};
        if(head == nullptr) {
            head = std::move(temp);
        } else {
            temp->next = std::move(head);
            head = std::move(temp);
        }
    }

    void print() {
        Data* current = head.get();
        while (current != nullptr) {
            std::cout << current->data << ", ";
            current = current->next.get();
        }
        std::cout << std::endl;
    }

    int getNumberOfRepetitions_iter(int data) {
        int counter{0};
        Data* current = head.get();
        while(current != nullptr) {
            if(current->data == data) counter++;
            current = current->next.get();
        }
        return counter;
    }

    int getNumberOfRepetitions_rec(int data) {
        int counter{0};
        Data* current = head.get();
        while(current != nullptr) {
            if(current->data == data) counter++;
            current = current->next.get();
        }
        return counter;
    }

    friend std::ostream& operator<<(std::ostream& os, const LinkedList& list);
};

std::ostream& operator<<(std::ostream& os, const LinkedList& list) {
    Data* current = list.head.get();
    while(current != nullptr) {
        os << current->data << ", ";
        current = current->next.get();
    }
    return os;
}

int main() {
    LinkedList list;
    for(int i = 0; i < 10; ++i) {
        list.push(i + 1);
    }
//    list.print();
//    std::cout << list << std::endl;
    std::cout << list.getNumberOfRepetitions_iter(1) << std::endl;
    return 0;
}