/**
 * Merge sort
 **/
#include <iostream>
#include <random>
#include "mergeSort.hpp"

int main() {
  LinkedList<int> list;
  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution{0,100};
  for (int i = 0; i < 10; ++i) {
    list.push(distribution(generator));
  }
  std::cout << list << std::endl;
  list.sort();
  std::cout << list << std::endl;
  //   std::cout << "Unsorted:\n" << list << std::endl;
  return 0;
}