#include <iostream>

using namespace std;

template <typename T>
struct Node {
  T data;
  Node<T>* next;

  Node(T data);
};

template<typename T>
void printList(Node<T>* head) {
  while(head != nullptr) {
    cout << head->data << ", ";
    head = head->next;
  }
  cout << endl;
}

template <typename ItemType>
class LinkedList {
 public:
  Node<ItemType>* m_head;
  LinkedList();

  void push(ItemType item);

  void sort() { mergeSort(&m_head); }

  void mergeSort(Node<ItemType>** head) {
    Node<ItemType>* left;
    Node<ItemType>* right;

    if (*head == nullptr || (*head)->next == nullptr) return;

    splitList(*head, &left, &right);
    mergeSort(&left);
    mergeSort(&right);
    *head = mergeSorted(left, right);
  }

  Node<ItemType>* mergeSorted(Node<ItemType>* left, Node<ItemType>* right) {
    Node<ItemType>* dummy{new Node<ItemType>(0)};
    Node<ItemType>* dummy_start = dummy;
    while (left != nullptr && right != nullptr) {
      if (left->data <= right->data) {
        dummy->next = left;
        left = left->next;
      } else {
        dummy->next = right;
        right = right->next;
      }
      dummy = dummy->next;
    }
    while (right != nullptr) {
      dummy->next = right;
      right = right->next;
      dummy = dummy->next;
    }
    while (left != nullptr) {
      dummy->next = left;
      left = left->next;
      dummy = dummy->next;
    }
    return dummy_start->next;
  }

  void splitList(Node<ItemType>* head, Node<ItemType>** left, Node<ItemType>** right) {
    Node<ItemType>* slow = head;
    Node<ItemType>* fast = head->next;

    while (fast != nullptr) {
      fast = fast->next;
      if (fast != nullptr) {
        fast = fast->next;
        slow = slow->next;
      }
    }

    *left = head;
    *right = slow->next;
    slow->next = nullptr;
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  const LinkedList<ItemType>& list) {
    Node<ItemType>* current = list.m_head;
    while (current != nullptr) {
      os << current->data << ", ";
      current = current->next;
    }
    return os;
  }
};

template <typename T>
Node<T>::Node(T data) : data{data}, next{nullptr} {}

template <typename ItemType>
LinkedList<ItemType>::LinkedList() : m_head{nullptr} {}

template <typename ItemType>
void LinkedList<ItemType>::push(ItemType item) {
  Node<ItemType>* temp{new Node<ItemType>(item)};
  if (m_head == nullptr) {
    m_head = temp;
  } else {
    temp->next = m_head;
    m_head = temp;
  }
}
