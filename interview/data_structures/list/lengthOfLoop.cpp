// Write a function detectAndCountLoop() that checks whether a given Linked
// List contains loop and if loop is present then returns count of nodes in
// loop. For example, loop is present in below linked list and length of loop
// is 4. If loop is not present, then function should return 0.

#include <iostream>
#include <memory>

struct Node {
  int data;
  Node* next;
  Node(int data) : data{data}, next{nullptr} {}
  ~Node() {
    if (next != nullptr) delete next;
  }
};

class LinkedList {
 private:
  Node* head;

 public:
  LinkedList() : head{nullptr} {}

  ~LinkedList() {
    if (head != nullptr) delete head;
  }

  void push(int data) {
    Node* temp{new Node(data)};
    if (head != nullptr) {
      temp->next = head;
      head = temp;
    } else {
      head = temp;
    }
  }

  void insert_loop(int loopIndex) {
    Node* current = head;
    Node* toLoop{nullptr};
    int counter{0};
    while (current->next != nullptr) {
      counter++;
      if (counter == loopIndex) {
        toLoop = current;
      }
      current = current->next;
    }
    current->next = toLoop;
  }

  int detectNumberOfElementsInLoop() {
       Node* slow = head;
       Node* fast = head->next;
       while(fast != nullptr && fast->next != nullptr) {
           if (fast == slow) {
               int loopCounter{0};
               std::cout << "Loop detected!" << std::endl;
                while(fast->next != slow) {
                    loopCounter++;
                    fast = fast->next;
                }
                return loopCounter;
           }
           fast = fast->next->next;
           slow = slow->next;
       }
       return 0;
    }

  friend std::ostream& operator<<(std::ostream& os, const LinkedList& node);
};

std::ostream& operator<<(std::ostream& os, const LinkedList& list) {
  Node* current = list.head;
  while (current != nullptr) {
    os << current->data << ", ";
    current = current->next;
  }
  return os;
}

int main() {
  LinkedList list;
  for (int i = 0; i < 10; ++i) {
    list.push(i);
  }
  std::cout << list << std::endl;
  list.insert_loop(4);
  std::cout << "Number: " << list.detectNumberOfElementsInLoop() << std::endl;
  return 0;
}