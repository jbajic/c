#include <iostream>
#include <memory>

using namespace std;

struct Node {
    int data;
    Node *next;

    Node(int data) : data{data}, next{nullptr} {}
};

class LinkedList {
private:
    Node *head;
public:
    LinkedList() : head{nullptr} {}

    void push(int data) {
        Node *temp{new Node{data}};
        if (head == nullptr) {
            head = temp;
        } else {
            temp->next = head;
            head = temp;
        };
    }

    void reverse() {
        Node *prev = nullptr;
        Node *current = head;
        Node *next = head;
        while (current != nullptr) {
            next = current->next;
            current->next = prev;
            prev = current;
            current = next;
        }
        head = prev;
    }

    friend ostream &operator<<(ostream &os, const LinkedList &list);
};

ostream &operator<<(ostream &os, const LinkedList &list) {
    Node *current = list.head;
    while (current != nullptr) {
        os << current->data << " ";
        current = current->next;
    }
    return os;
}

int main() {
    LinkedList list;
    for (int i = 0; i < 10; i++) {
        list.push(i);
    }
    cout << list << endl;
    list.reverse();
    cout << list << endl;
    return 0;
}