/*
 * Given a singly linked list, find middle of the linked list. For example, if given linked list is 1->2->3->4->5 then output should be 3.
 * If there are even nodes, then there would be two middle nodes, we need to print second middle element. For example, if given linked list is 1->2->3->4->5->6 then output should be 4.
 */
#include <iostream>

using namespace std;

struct Data{
    int data;
    Data* next = nullptr;
    Data(int data): data{data} {}
    ~Data() {
        if(next != nullptr) {
            delete next;
        }
    }
};

class LinkedList{
private:
    Data* head = nullptr;
    int size = 0;
public:
    void insert(Data* data) {
        if(head == nullptr) {
            head = data;
        } else {
            Data* current = head;
            while(current->next != nullptr) {
                current = current->next;
            }
            current->next = data;
        };
        size++;
    }

    void print() {
        Data* current = head;
        while(current != nullptr) {
            cout << current->data << ", ";
            current = current->next;
        }
        cout << endl;
    }

    // Two pointers method
    Data* getMiddleElement() {
        if(head == nullptr)
            return nullptr;
        Data* slow = head;
        Data* fast = head;
        while(fast != nullptr && fast->next != nullptr) {
            slow = slow->next;
            fast = fast->next->next;
        }
        return slow;
    }
};

int main() {
    LinkedList list;
    for(int i = 0; i< 3; ++i) {
        Data* data = new Data(i);
        list.insert(data);
    }
    Data* middle = list.getMiddleElement();
    cout << "Middle is " << middle->data << endl;
    list.print();
    return 0;
}