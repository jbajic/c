// Given a linked list, check if the linked list has loop or not. Below diagram
// shows a linked list with a loop.
#include <iostream>
#include <memory>
#include <unordered_set>

using namespace std;

struct Node {
  int data;
  Node* next;
  Node(int data) : data{data}, next{nullptr} {}
  ~Node() {
    if (next != nullptr) {
      cout << "Deleting " << data << endl;
      delete next;
    }
  }
};

class LinkedList {
 private:
  Node* head;

 public:
  LinkedList() : head{nullptr} {}

  ~LinkedList() { delete head; }

  void push(int data) {
    Node* temp = new Node(data);
    if (head == nullptr) {
      head = temp;
    } else {
      temp->next = head;
      head = temp;
    }
  }

  bool isThereLoop() {
    unordered_set<Node*> hashMap;
    Node* current = head;
    while (current != nullptr) {
      if (hashMap.find(current) != hashMap.end()) return true;
      hashMap.insert(current);
      current = current->next;
    }
    return false;
  }

  bool floyd_cycle_detection() {
    Node* slow = head;
    Node* fast = head;
    while(fast != nullptr && fast->next != nullptr) {
      slow = slow->next;
      fast = fast->next->next;
      if(slow == fast) {
        return true;
      }
    }
    return false;
  }

  void insert_loop(int data) {
    Node* current = head;
    Node* toLoop{nullptr};
    int counter{0};
    while (current->next != nullptr) {
      counter++;
      if (counter == 5) {
        toLoop = current;
      }
      current = current->next;
    }
    current->next = toLoop;
  }

  friend ostream& operator<<(ostream& os, const LinkedList& list);
};

ostream& operator<<(ostream& os, const LinkedList& list) {
  Node* current = list.head;
  while (current != nullptr) {
    os << current->data << ", ";
    current = current->next;
  }
  return os;
}

int main() {
  LinkedList list;
  for (int i = 0; i < 10; ++i) {
    list.push(i);
  }
  list.insert_loop(2);

  if (list.floyd_cycle_detection()) {
    cout << "Loop detected!" << endl;
    exit(-1);
  }
  cout << list << endl;
  return 0;
}
