/**
Like Merge Sort, QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the given
array around the picked pivot. There are many different versions of quickSort that pick pivot in different ways.

    Always pick first element as pivot.
    Always pick last element as pivot (implemented below)
    Pick a random element as pivot.
    Pick median as pivot.

The key process in quickSort is partition(). Target of partitions is, given an array and an element x of array as pivot, 
put x at its correct position in sorted array and put all smaller elements (smaller than x) before x, and put all
greater elements (greater than x) after x. All this should be done in linear time.
    *
    * Worst complexity: n*log(n)
    * Average complexity: n*log(n)
    * Best complexity: n*log(n)
    * Space complexity: n
 * */
#include <iostream>
#include "base.hpp"

using namespace std;

using sorting::printArray;
using sorting::randomizeArray;
using sorting::swap;

constexpr int numberOfElements = 10;

void quickSort(int arr[], int lowIndex, int highIndex);
int partition(int arr[], int lowIndex, int highIndex);

int main() {
    int arrayOfNumbers[numberOfElements];

    randomizeArray(arrayOfNumbers, numberOfElements);
    printArray(arrayOfNumbers, numberOfElements);

    quickSort(arrayOfNumbers, 0, numberOfElements);
    cout << "\n";
    printArray(arrayOfNumbers, numberOfElements);
    return 0;
}

void quickSort(int arr[], int lowIndex, int highIndex) {
    if (lowIndex < highIndex) {
        int pivot_index = partition(arr, lowIndex, highIndex);

        quickSort(arr, lowIndex, pivot_index);
        quickSort(arr, pivot_index + 1, highIndex);
    }
}

// void quickSort(int array[], int low, int high)
// {
//     if (high > low)
//     {
//         int pivotIndex = partition(array, low, high);

//         quickSort(array, low, pivotIndex - 1);
//         quickSort(array, pivotIndex + 1, high);
//     }
// }

// int partition(int arr[], int lowIndex, int highIndex) {
//     // Always pick first element
//     int pivotIndex = lowIndex;
//     for(int i = lowIndex; i < highIndex; ++i) {
//         if(arr[i] < arr[pivotIndex]) {
//             int temp = arr[i];
//             for(int j = i; j > lowIndex; --j) {
//                 arr[j] = arr[j - 1]; 
//             }
//             arr[lowIndex] = temp;
//             pivotIndex++;
//         }
//     }
//     return pivotIndex;
// }

// int partition(int array[], int low, int high)
// {
//     int leftIndex = low;
//     int rightIndex = high + 1;
//     int pivot = array[low];

//     do
//     {
//         do
//         {
//             leftIndex++;
//         } while (array[leftIndex] <= pivot);
//         do
//         {
//             rightIndex--;
//         } while (array[rightIndex] > pivot);
//         if (leftIndex < rightIndex)
//             std::swap(array[leftIndex], array[rightIndex]);
//     } while (leftIndex < rightIndex);

//     std::swap(array[low], array[rightIndex]);
//     return rightIndex;
// }

/**
 * Pivot is last element
 * */
// int partition(int array[], int low, int high)
// {
//     int pivot = array[high];
//     int pivotIndex = low;

//     for(int i = low; i <= high -1; ++i) {
//         if(array[i] <= pivot) {
//             std::swap(array[i], array[pivotIndex]);
//             pivotIndex++;
//         }
//     }
//     std::swap(array[pivotIndex], array[high]);
//     return pivotIndex;
// }