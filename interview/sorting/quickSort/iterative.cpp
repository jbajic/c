/**
Like Merge Sort, QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the given
array around the picked pivot. There are many different versions of quickSort that pick pivot in different ways.

    Always pick first element as pivot.
    Always pick last element as pivot (implemented below)
    Pick a random element as pivot.
    Pick median as pivot.

The key process in quickSort is partition(). Target of partitions is, given an array and an element x of array as pivot, 
put x at its correct position in sorted array and put all smaller elements (smaller than x) before x, and put all
greater elements (greater than x) after x. All this should be done in linear time.
    *
    * Worst complexity: n*log(n)
    * Average complexity: n*log(n)
    * Best complexity: n*log(n)
    * Space complexity: n
 * */
#include <iostream>
#include "base.hpp"

using namespace std;

using sorting::printArray;
using sorting::randomizeArray;
using sorting::swap;

constexpr int numberOfElements = 10;

void quickSort(int arr[], int lowIndex);
int partition(int arr[], int lowIndex, int highIndex);

int main() {
    int arrayOfNumbers[numberOfElements];

    randomizeArray(arrayOfNumbers, numberOfElements);
    printArray(arrayOfNumbers, numberOfElements);

    quickSort(arrayOfNumbers, numberOfElements - 1);
    cout << "\n";
    printArray(arrayOfNumbers, numberOfElements);
    return 0;
}

void quickSort(int arr[], int numberOfElements) {
    int pivotIndex = 0;
    while(pivotIndex != numberOfElements - 1) {
        int pivot = arr[pivotIndex];
        for(int i = pivotIndex + 1; i < numberOfElements; ++i) {
            if(arr[i] <= arr[pivotIndex]) {
                for(int j = 0; j <= i; j++) {
                    
                }
            }
        }
    }
}
