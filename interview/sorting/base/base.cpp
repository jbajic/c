#include "base.hpp"

namespace sorting
{

void randomizeArray(int *arr, int numberOfElements, int offset)
{
    std::random_device rd;                                               // obtain a random number from hardware
    std::mt19937 eng(rd());                                              // seed the generator
    std::uniform_int_distribution<> distr(0, numberOfElements + offset); // define the range

    for (int i = 0; i < numberOfElements; ++i)
    {
        arr[i] = distr(eng);
    }
}

void swap(int *numOne, int *numTwo)
{
    int temp = *numOne;
    (*numOne) = (*numTwo);
    (*numTwo) = temp;
}


void printArray(int *arr, int numberOfElements)
{
    for(int i = 0; i < numberOfElements; ++i) {
        std::cout << arr[i] << ", ";
    }
    std::cout << std::endl;
}
} // namespace sorting
