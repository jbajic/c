#include <random>
#include <iostream>

namespace sorting
{

void randomizeArray(int *arr, int numberOfElements, int offset = 100);

void swap(int *numOne, int *numTwo);

void printArray(int *arr, int numberOfElements);

} // namespace sorting
