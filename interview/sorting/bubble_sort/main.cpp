/**
    * Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list,
    * compares adjacent elements and swaps them if they are in the wrong order. The pass through the list is repeated until
    * the list is sorted.
    *
    * Worst complexity: n^2
    * Average complexity: n^2
    * Best complexity: n
    * Space complexity: 1
 * */
#include <iostream>
#include "base.hpp"

using namespace std;

using sorting::printArray;
using sorting::randomizeArray;
using sorting::swap;

constexpr int numberOfElements = 20;

int main() {
    std::ios::sync_with_stdio(false);
    int arrayOfNumbers[numberOfElements];

    randomizeArray(arrayOfNumbers, numberOfElements);
    printArray(arrayOfNumbers, numberOfElements);

    bool isSorted;
    do {
        isSorted = true;
        for (int i = 0; i < numberOfElements - 1; ++i) {
            if (arrayOfNumbers[i] > arrayOfNumbers[i + 1]) {
                int temp = arrayOfNumbers[i];
                arrayOfNumbers[i] = arrayOfNumbers[i + 1];
                arrayOfNumbers[i + 1] = temp;
                isSorted = false;
            }
        }
    } while (!isSorted);

    printArray(arrayOfNumbers, numberOfElements);
    return 0;
}