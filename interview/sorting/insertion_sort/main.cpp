/**
    * Insertion sort is a simple sorting algorithm that builds the final sorted array (or list) one item at a time. It is much less efficient
    * on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort.
    *
    * Worst complexity: n^2
    * Average complexity: n^2
    * Best complexity: n
    * Space complexity: 1
 * */
#include <iostream>
#include "base.hpp"

using namespace std;

using sorting::printArray;
using sorting::randomizeArray;
using sorting::swap;

constexpr int numberOfElements = 20;

int main(int argc, char *argv[])
{
    int arrayOfNumbers[numberOfElements];
    randomizeArray(arrayOfNumbers, numberOfElements);
    printArray(arrayOfNumbers, numberOfElements);

    for (int i = 1; i < numberOfElements; i++)
    {
        int j;
        for (j = i; j >= 1; --j)
        {
            if (arrayOfNumbers[j - 1] > arrayOfNumbers[j])
            {
                swap(&arrayOfNumbers[j - 1], &arrayOfNumbers[j]);
            }
            else
            {
                break;
            }
        }
        cout << "Switch: " << arrayOfNumbers[j - 1] << " with: " << arrayOfNumbers[j] << endl;
    }

    printArray(arrayOfNumbers, numberOfElements);

    return 0;
}