/**
 * Like QuickSort, Merge Sort is a Divide and Conquer algorithm. It divides
 * input array in two halves, calls itself for the two halves and then merges
 * the two sorted halves. The merge() function is used for merging two halves.
 * The merge(arr, l, m, r) is key process that assumes that arr[l..m] and
 * arr[m+1..r] are sorted and merges the two sorted sub-arrays into one. See
 * following C implementation for details.
 *
 * Worst complexity: n*log(n)
 * Average complexity: n*log(n)
 * Best complexity: n*log(n)
 * Space complexity: n
 * */
#include <iostream>
#include "base.hpp"

using namespace std;

using sorting::printArray;
using sorting::randomizeArray;
using sorting::swap;

constexpr int numberOfElements = 5;

void mergeSort(int arr[], int l, int r);
void merge(int arr[], int l, int r, int m);

int main()
{
    std::ios::sync_with_stdio(false);
    int arrayOfNumbers[numberOfElements];

    randomizeArray(arrayOfNumbers, numberOfElements);
    printArray(arrayOfNumbers, numberOfElements);

    mergeSort(arrayOfNumbers, 0, numberOfElements - 1);
    cout << "\n";
    printArray(arrayOfNumbers, numberOfElements);
    return 0;
}

void mergeSort(int arr[], int l, int r)
{
    if (l < r)
    {
        int m{(r + l) / 2};
        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);
        merge(arr, l, r, m);
    }
}

void merge(int arr[], int l, int r, int m)
{
    cout << l << ", " << r << ", " << m << endl;
    // exit(0);
    int len1{m - l + 1};
    int len2{r - m};
    int Left[len1], Right[len2];

    for (int i{0}; i < len1; ++i)
    {
        Left[i] = arr[l + i];
    }
    for (int i{0}; i < len2; ++i)
    {
        Right[i] = arr[m + i + 1];
    }
    int count1{0}, count2{0};
    while (count1 < len1 && count2 < len2)
    {
        if (Left[count1] <= Right[count2])
        {
            arr[l + count1 + count2] = Left[count1];
            count1++;
        }
        else
        {
            arr[l + count1 + count2] = Right[count2];
            count2++;
        }
    }
    while (count1 < len1)
    {
        arr[l + count1 + count2] = Left[count1];
        count1++;
    }
    while (count2 < len2)
    {
        arr[l + count1 + count2] = Right[count2];
        count2++;
    }
}